<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('TaxType');?>
	<fieldset>
 		<legend><?php __('Edit Jenis Pajak');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Jenis Pajak');?>:</td>
                <td><?php echo $form->input('code', array('div'=>false, 'label' => false, 'maxlength' => 20, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Potongan Pajak');?>:</td>
                <td>
                    <?php 
                        echo $form->input('amount', array('div'=>false, 'label' => false, 'maxlength' => 4, 'size' => 5, 'class'=>'required'));
                    ?>&nbsp; %
                    <br />
                    <span class="label">Decimal dengan titik, bukan koma</span>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Deskripsi');?>:</td>
                <td><?php echo $form->input('name', array('div'=>false, 'label' => false, 'maxlength' => 100, 'class'=>'required', 'type' => 'textarea'));?></td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Save', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Delete', true), array('action'=>'delete', $this->data['TaxType']['id']), array('class'=>'del'), sprintf(__('Are you sure you want to delete', true) . ' %s?', $this->data['TaxType']['name'])) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
