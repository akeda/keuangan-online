<?php $javascript->link('jquery.validate.min', false); ?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('CashReceiver');?>
	<fieldset>
 		<legend><?php __('Tambah penerima kas untuk SPTB');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Penerima');?>:</td>
                <td>
                <?php 
                    echo $form->input('name', array(
                        'div'=>false, 'label' => false, 'type' => 'textarea', 'class'=>'required'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Save', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Delete', true), array('action'=>'delete', $this->data['CashReceiver']['id']), array('class'=>'del'), sprintf(__('Are you sure you want to delete', true) . ' dana dari %s?', $this->data['CashReceiver']['name'])) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
