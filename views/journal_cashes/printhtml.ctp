<h1>Buku Kas Tunai</h1>
<span><?php echo $monthname . ', ' . $year;?> &mdash; * adalah bukti penerimaan</span>
<table>
    <thead>
        <tr>
            <th>Tanggal</th>
            <th>Uraian</th>
            <th>Nomor Tanda Bukti Kas</th>
            <th>Penerimaan<br />(Rp.)</th>
            <th>Pengeluaran<br />(Rp.)</th>
            <th>Sisa<br />(Rp.)</th>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($total) && isset($records[0])): ?>
            <tr>
                <td>
                    <?php
                        echo $time->format('j', 
                            $records[0]['JournalCash']['transaction_date']);
                    ?>
                </td>
                <td>Saldo <?php echo $time->format('d/m/Y', $ldaylmonth);?></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    <?php
                        $total['balance'] = $total['balance']*1;
                        echo ($total['balance']) ? number_format($total['balance'],2,'.',',') : '-';
                    ?>
                </td>
            </tr>
        <?php endif;?>
        <?php $tOut = $tIn = 0;?>
        <?php foreach ($records as $record): ?>
            <tr>
                <td>
                    <?php
                        echo $time->format('j', 
                            $record['JournalCash']['transaction_date']);
                    ?>
                </td>
                <td>
                    <?php 
                        echo $record['JournalCash']['description'];
                    ?>
                </td>
                <td>
                    <?php 
                        if ( $record['JournalCash']['debit_amount']*1 ) {
                            echo $record['JournalCash']['cash_receipt_no_in'] ? $record['JournalCash']['cash_receipt_no_in'] . ' *' : '';
                        } else {
                            echo $record['JournalCash']['cash_receipt_no'];
                        }
                    ?>
                </td>
                <td>
                    <?php
                        if ( ($record['JournalCash']['debit_amount']*1) ) {
                            echo number_format($record['JournalCash']['debit_amount'],2,'.',',');
                            $tIn += $record['JournalCash']['debit_amount'];
                        }
                    ?>
                </td>
                <td>
                    <?php 
                        if ( ($record['JournalCash']['credit_amount']*1) ) {
                            echo number_format($record['JournalCash']['credit_amount'],2,'.',',');
                            $tOut += $record['JournalCash']['credit_amount'];
                        }
                    ?>
                </td>
                <td></td>
            </tr>
        <?php endforeach; ?>
        <?php if (!empty($records)): ?>
            <tr>
            <td>&nbsp;</td>
            <td colspan="2">Total</td>
            <?php
                if (isset($total['balance'])) {
                    $tIn += $total['balance'];
                }
            ?>
            <td><?php echo number_format($tIn,2,'.',',');?></td>
            <td><?php echo number_format($tOut,2,'.',',');?></td>
            <td><?php echo number_format(($tIn - $tOut),2,'.',',');?></td>
            </tr>
        <?php endif;?>
    </tbody>
</table>