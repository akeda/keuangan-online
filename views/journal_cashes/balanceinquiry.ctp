<fieldset>
    <legend>Informasi Saldo Kas</legend>
    <span>Saldo Efektif <?php echo $date_now;?>:</span>
    <strong>
        <?php 
            echo 'Rp ' . number_format(
                $balanceinquiry, 2,
                '.', ','
            );
        ?>
    </strong>
</fieldset>