<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('ActivityChild');?>
	<fieldset>
 		<legend><?php __('Tambah MAK');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Kode MAK');?>:</td>
                <td><?php echo $form->input('code', array('div'=>false, 'label' => false, 'maxlength' => 10, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Deskripsi');?>:</td>
                <td><?php echo $form->input('name', array('div'=>false, 'label' => false, 'maxlength' => 100, 'class'=>'required', 'type' => 'textarea'));?></td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Add', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>