<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array("fields" => array(
                "code" => array(
                    'title' => __("Kode", true),
                    'sortable' => true
                ),
                "name" => array(
                    'title' => __("Deskripsi", true), 
                    'sortable' => true
                ),
                "created_by" => array(
                    'title' => __("Created By", true),
                    'sortable' => false
                ),
                "created" => array(
                    'title' => __("Created On", true),
                    'sortable' => false
                )
              ),
              "editable"  => "code",
              "assoc" => array(
                'created_by' => array(
                    'field' => 'name',
                    'model' => 'User'
                )
              ),
              "displayedAs" => array(
                'created' => 'datetime'
              )
        ));
?>
</div>