<h1><?php echo $u . ' &mdash; Tahun ' . $year;?></h1>
<br />
<table>
    <thead>
        <tr>
            <th>No.</th>
            <th class="center">Kegiatan</th>
            <th class="center">Pagu Anggaran</th>
            <th class="center">Realisasi</th>
            <th class="center">Prosentase Penggunaan</th>
            <th class="center">Sisa Anggaran</th>
        </tr>
    </thead>
    <tbody>
    <?php $no = 1; ?>
    <?php for ($i = 1; $i <= 3; $i++) { ?>
    <?php foreach (${'t'.$i} as $r): ?>
    <tr>
        <td><?php echo $no++;?></td>
        <td><?php echo $r['activity'];?></td>
        <td class="numeric"><?php echo number_format($r['budget'],2,'.',',');?></td>
        <td class="numeric"><?php echo number_format($r['realization'],2,'.',',');?></td>
        <td class="numeric"><?php echo number_format($r['percent'],2,'.',',');?> %</td>
        <td class="numeric"><?php echo number_format($r['remain'],2,'.',',');?></td>
    </tr>
    <?php endforeach;?>
    <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <th colspan="2" class="numeric">Total</th>
            <th class="numeric"><?php echo number_format($total,2,'.',',');?></th>
            <th class="numeric"><?php echo number_format($total_r,2,'.',',');?></th>
            <th class="numeric">
            <?php 
                echo number_format($total_percent,2,'.',',') . ' %';
            ?>
            </th>
            <th class="numeric">
            <?php 
                echo number_format($total_remain,2,'.',',');?>
            </th>
        </tr>
    </tfoot>
</table>