<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('Budget', array('action' => 'cpdprinthtml'));?>
	<fieldset>
 		<legend><?php echo 'Komposisi Anggaran';?></legend>
        <table class="input">
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Tahun');?>:</td>
                <td>
                    <?php
                        echo $form->year('y', '2008', date('Y'), date('Y'));
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Print', true), array('div'=>false, 'id' => 'print'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>