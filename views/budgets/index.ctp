<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array("fields" => array(
                "budget_year" => array(
                    'title' => __("Tahun Anggaran", true), 
                    'sortable' => true
                ),
                "activity_id2" => array(
                    'title' => __("Kode", true), 
                    'sortable' => true
                ),
                "activity_id" => array(
                    'title' => __("Sub Kegiatan", true), 
                    'sortable' => true
                ),
                "total" => array(
                    'title' => __("Alokasi", true), 
                    'sortable' => false,
                    'wrapBefore' => '<span class="numeric_on_grid">',
                    'wrapAfter' => '</span>'
                ),
                "unit_code_id" => array(
                    'title' => __("Unit Kerja", true),
                    'sortable' => true
                ),
                "created_by" => array(
                    'title' => __("Created By", true),
                    'sortable' => false
                ),
                "created" => array(
                    'title' => __("Created On", true),
                    'sortable' => false
                )
              ),
              "editable"  => "activity_id",
              "assoc" => array(
                'activity_id2' => array(
                    'field' => 'code',
                    'model' => 'Activity'
                ),
                'activity_id' => array(
                    'field' => 'name',
                    'model' => 'Activity'
                ),
                'unit_code_id' => array(
                    'field' => 'name',
                    'model' => 'UnitCode'
                ),
                'created_by' => array(
                    'field' => 'name',
                    'model' => 'User'
                )
              ),
              'displayedAs' => array(
                'created' => 'datetime'
              )
        ));
?>
</div>