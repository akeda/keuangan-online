<?php echo $javascript->codeBlock($ajaxURL);?>
<?php echo $javascript->link('jquery.numeric', false);?>
<?php echo $javascript->link('budget_details', false);?>
<?php echo $html->css('budget_details', 'stylesheet', array('inline' => false));?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Budget', array('action' => 'edit'));?>
	<fieldset>
 		<legend><?php __('Edit Anggaran');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Tahun Anggaran', true);?></td>
                <td>
                    <?php
                        echo $form->input('budget_year', array(
                            'div' => false, 'label' => false,
                            'class' => 'inputText',
                            'size' => 4
                        ));
                    ?>
                </td>
            </td>
            <tr>
                <td class="label-required"><?php echo __('Kegiatan', true);?></td>
                <td>
                <?php
                    echo $form->select('activity_parent', $activities, $parent_id, array(
                        'class' => 'inputText ajax_select',
                        'rel' => '#BudgetActivityId',
                        'ref' => 'parent_id'
                    ), 'Pilih Kegiatan');
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Sub Kegiatan', true);?></td>
                <td>
                    <?php
                        echo $form->select('activity_id', $subactivities, $activity_id, array(
                            'class' => 'inputText',
                            'model' => 'Activity',
                            'field' => 'name'
                        ), 'Pilih Kegiatan Dahulu');
                        echo ($form->isFieldError('activity_id')) ? $form->error('activity_id') : '';
                    ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Unit Kerja Pelaksana</td>
                <td>
                    <?php
                        echo $form->select('unit_code_id', $unit_codes, null, array(
                            'class' => 'inputText'
                        ), false);
                        echo ($form->isFieldError('unit_code_id')) ? $form->error('unit_code_id') : '';
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table id="budget_details" class="budget_details">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>MAK</th>
                                <th colspan="2" class="center">Rincian</th>
                                <th>Volume</th>
                                <th>Satuan</th>
                                <th>Harga Satuan</th>
                                <th>Sumber Dana</th>
                                <th>Total Alokasi</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if ( isset($budgets) ): ?>
                        <?php
                            $total_activity = 0;
                            foreach ($budgets['BudgetDetail'] as $key => $budgetDetail):
                                $total_allocated = 0;
                                $total = 0;

                                echo '<tr class="row_budget_details" id="r'.$key.'" rel="r'.$key.'_d0">';
                                    echo '<td><input type="checkbox" name="data[Budget][BudgetDetail]['.$key.'][id]" class="cb_budget_details" class="inputText" /></td>';
                                    echo '<td>';
                                    echo $form->select(
                                    'activity_child_id', $activity_children,
                                        $budgetDetail['activity_child_id'],
                                    array(
                                        'id' => null,
                                        'class' => 'activity_childs',
                                        'name' => 'data[Budget][BudgetDetail]['.$key.'][activity_child_id]'
                                    ),
                                    false);
                                    echo '</td>';
                                    echo '<td>';
                                    echo $html->image('cross.png', array(
                                            'alt' => 'Klik untuk menghapus rincian',
                                            'rel' => 'r'.$key.'_d0',
                                            'class' => 'del_description'
                                        ));
                                    echo '</td>';
                                    echo '<td>';
                                    echo $form->input('description',
                                            array(
                                                'id' => null,
                                                'name' => 'data[Budget][BudgetDetail]['.$key.'][BudgetDetailDescription][0][description]',
                                                'class' => 'inputText description',
                                                'type' => 'textarea', 'div' => false, 'label' => false,
                                                'value' => $budgetDetail['BudgetDetailDescription'][0]['description']
                                            )
                                        );
                                    echo '</td>';
                                    echo '<td>';
                                    echo $form->select('volume_id', $volumes,
                                            $budgetDetail['BudgetDetailDescription'][0]['volume_id'],
                                        array(
                                            'name' => 'data[Budget][BudgetDetail]['.$key.'][BudgetDetailDescription][0][volume_id]',
                                            'class' => 'inputText volume_id', 'id' => null
                                        ), false);
                                    echo '</td>';
                                    echo '<td>';
                                    echo $form->input('unit_count', array(
                                            'div' => false, 'label' => false, 'size' => 5,
                                            'id' => null, 'class' => 'inputText unit_count numeric',
                                            'name' => 'data[Budget][BudgetDetail]['.$key.'][BudgetDetailDescription][0][unit_count]',
                                            'value' => number_format(
                                                $budgetDetail['BudgetDetailDescription'][0]['unit_count'],
                                                2, '.', ','
                                            )
                                        ));
                                    echo '</td>';
                                    echo '<td>';
                                    echo $form->input('unit_amount', array(
                                            'div' => false, 'label' => false, 'id' => null,
                                            'class' => 'inputText unit_amount numeric',
                                            'name' => 'data[Budget][BudgetDetail]['.$key.'][BudgetDetailDescription][0][unit_amount]',
                                            'value' => number_format(
                                                $budgetDetail['BudgetDetailDescription'][0]['unit_amount'],
                                                2, '.', ','
                                            )
                                        ));
                                    echo '</td>';
                                    $total_allocated = $budgetDetail['BudgetDetailDescription']
                                        [0]['unit_count'] * $budgetDetail['BudgetDetailDescription']
                                        [0]['unit_amount'];
                                    $total += $total_allocated;

                                    echo '<td>';
                                    echo $form->select('fund_source',
                                        $fund_sources, $budgetDetail['BudgetDetailDescription'][0]['fund_source'], array(
                                            'id' => null,
                                            'class' => 'inputText fund_source',
                                            'name' => 'data[Budget][BudgetDetail]['.$key.']' .
                                                '[BudgetDetailDescription][0][fund_source]'
                                    ), '');
                                    echo '</td>';
                                    echo '<td><span class="total_allocated">' .
                                        number_format($total_allocated, 2, '.', ',') .
                                        '</span></td>';
                                echo '</tr>';

                                // cek budget_detail_description and make sure > 2
                                if (!empty($budgetDetail['BudgetDetailDescription']) && count($budgetDetail['BudgetDetailDescription']) > 1) {
                                    // iterate each description
                                    foreach ($budgetDetail['BudgetDetailDescription'] as $key2 => $budgetDetailDesc):
                                        if ($key2 === 0) {
                                            continue;
                                        }

                                        echo '<tr class="row_budget_detail_descriptions" id="r'.$key.'_d'.$key2.'" rel="r'.$key.'_d'.$key2.'">';
                                        echo '<td class="empty">&nbsp;</td>';
                                        echo '<td class="empty">&nbsp;</td>';
                                        echo '<td>';
                                        echo $html->image('cross.png', array(
                                                'alt' => 'Klik untuk menghapus rincian',
                                                'rel' => 'r'.$key.'_d'.$key2,
                                                'class' => 'del_description'
                                            ));
                                        echo '</td>';
                                        echo '<td>';
                                        echo $form->input('description',
                                                array(
                                                    'id' => null,
                                                    'name' => 'data[Budget][BudgetDetail]['.$key.'][BudgetDetailDescription]['.$key2.'][description]',
                                                    'class' => 'inputText description',
                                                    'type' => 'textarea', 'div' => false, 'label' => false,
                                                    'value' => $budgetDetailDesc['description']
                                                )
                                            );
                                        echo '</td>';
                                        echo '<td>';
                                        echo $form->select('volume_id', $volumes,
                                                $budgetDetailDesc['volume_id'],
                                            array(
                                                'name' => 'data[Budget][BudgetDetail]['.$key.'][BudgetDetailDescription]['.$key2.'][volume_id]',
                                                'class' => 'inputText volume_id', 'id' => null
                                            ), false);
                                        echo '</td>';
                                        echo '<td>';
                                        echo $form->input('unit_count', array(
                                                'div' => false, 'label' => false, 'size' => 5,
                                                'id' => null, 'class' => 'inputText unit_count numeric',
                                                'name' => 'data[Budget][BudgetDetail]['.$key.'][BudgetDetailDescription]['.$key2.'][unit_count]',
                                                'value' => number_format(
                                                    $budgetDetailDesc['unit_count'],
                                                    2, '.', ','
                                                )
                                            ));
                                        echo '</td>';
                                        echo '<td>';
                                        echo $form->input('unit_amount', array(
                                                'div' => false, 'label' => false, 'id' => null,
                                                'class' => 'inputText unit_amount numeric',
                                                'name' => 'data[Budget][BudgetDetail]['.$key.'][BudgetDetailDescription]['.$key2.'][unit_amount]',
                                                'value' => number_format(
                                                    $budgetDetailDesc['unit_amount'],
                                                    2, '.', ','
                                                )
                                            ));
                                        echo '</td>';
                                        $total_allocated = $budgetDetailDesc['unit_count'] * $budgetDetailDesc['unit_amount'];
                                        $total += $total_allocated;

                                        echo '<td>';
                                        echo $form->select('fund_source',
                                            $fund_sources, $budgetDetailDesc['fund_source'], array(
                                                'id' => null,
                                                'class' => 'inputText fund_source',
                                                'name' => 'data[Budget][BudgetDetail]['.$key.']' .
                                                    '[BudgetDetailDescription]['.$key2.'][fund_source]'
                                        ), '');
                                        echo '</td>';
                                        echo '<td><span class="total_allocated">'.
                                            number_format($total_allocated, 2, '.', ',') .
                                            '</span></td>';
                                        echo '</tr>';
                                    endforeach;
                                } // eof check BudgetDetailDesc

                                // add .add_description row (latest row)
                                echo '<tr id="rg'.$key.'" class="add_description_row">' .
                                        '<td colspan="2">&nbsp;</td>
                                        <td colspan="2">
                                            <span class="up"></span>
                                            <input type="button" class="add_description" id="b'.$key.'" value="+ Tambah rincian" />
                                        </td>
                                        <td colspan="4" class="numeric">
                                            <span class="total_label">Total</span>
                                        </td>
                                        <td class="numeric">
                                            <span class="total">'. number_format($total,2,'.', ',') .
                                            '</span>
                                            <span class="up"></span>
                                        </td>' .
                                     '</tr>';
                                $total_activity += $total;
                            endforeach; // eof BudgetDetail
                        ?>
                        <?php else: ?>
                            <tr class="row_budget_details" id="r0" rel="r0_d0">
                                <td>
                                    <input type="checkbox" name="data[Budget][BudgetDetail][0][id]" class="cb_budget_details" class="inputText" />
                                </td>
                                <td>
                                    <?php
                                        echo $form->select(
                                        'activity_child_id', $activity_children,
                                            null,
                                        array(
                                            'id' => null,
                                            'class' => 'activity_childs',
                                            'name' => 'data[Budget][BudgetDetail][0][activity_child_id]'
                                        ),
                                        false);
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        echo $html->image('cross.png', array(
                                            'alt' => 'Klik untuk menghapus rincian',
                                            'rel' => 'r0_d0',
                                            'class' => 'del_description'
                                        ));
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        echo $form->input('description',
                                            array(
                                                'id' => null,
                                                'name' => 'data[Budget][BudgetDetail][0][BudgetDetailDescription][0][description]',
                                                'class' => 'inputText description',
                                                'type' => 'textarea', 'div' => false, 'label' => false
                                            )
                                        );
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        echo $form->select('volume_id', $volumes,
                                            null,
                                        array(
                                            'name' => 'data[Budget][BudgetDetail][0][BudgetDetailDescription][0][volume_id]',
                                            'class' => 'inputText volume_id', 'id' => null
                                        ), false);
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        echo $form->input('unit_count', array(
                                            'div' => false, 'label' => false, 'size' => 5,
                                            'id' => null, 'class' => 'inputText unit_count numeric',
                                            'name' => 'data[Budget][BudgetDetail][0][BudgetDetailDescription][0][unit_count]'
                                        ));
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        echo $form->input('unit_amount', array(
                                            'div' => false, 'label' => false, 'id' => null,
                                            'class' => 'inputText unit_amount numeric',
                                            'name' => 'data[Budget][BudgetDetail][0][BudgetDetailDescription][0][unit_amount]'
                                        ));
                                    ?>
                                </td>
                                <td>
                                <?php
                                    echo $form->select('fund_source',
                                        $fund_sources, null, array(
                                            'id' => null,
                                            'class' => 'inputText fund_source',
                                            'name' => 'data[Budget][BudgetDetail][0]' .
                                                '[BudgetDetailDescription][0][fund_source]'
                                    ), '');
                                ?>
                                </td>
                                <td><span class="total_allocated"></span></td>
                            </tr>
                            <tr id="rg0" class="add_description_row">
                                <td colspan="2">&nbsp;</td>
                                <td colspan="2">
                                    <span class="up"></span>
                                    <input type="button" class="add_description" id="b0" value="+ Tambah rincian" />
                                </td>
                                <td colspan="4" class="numeric">
                                    <span class="total_label">Total</span>
                                </td>
                                <td class="numeric">
                                    <span class="total"></span>
                                    <span class="up"></span>
                                </td>
                            </tr>
                        <?php endif;?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="2">
                                    <span class="up"></span>
                                    <input type="button" name="add_row" id="add_row" value="+ Tambah MAK" /> &nbsp; atau &nbsp;
                                    <input type="button" name="del_row" id="del_row" value="- Hapus MAK" />
                                </th>
                                <th colspan="6" class="numeric">
                                    <span class="total_label">Total</span>
                                </th>
                                <th class="numeric">
                                    <span id="total_for_activity">
                                    <?php
                                        if (isset($total_activity)):
                                            echo number_format($total_activity, 2, '.', ',');
                                        endif
                                    ?>
                                    </span>
                                    <span class="up"></span>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Simpan Anggaran ini', array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>