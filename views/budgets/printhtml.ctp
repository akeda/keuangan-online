<h1>Rekapitulasi Pagu Anggaran <?php echo $year;?></h1>
<?php if ($unmatch):?>
    <br />
    <span>* Ada beberapa transaksi yang belum terkait dengan MAK pada anggaran tahun <?php echo $year;?> dengan total
        <?php echo number_format($total_unmatch,2,'.',',');?>. Berikut MAK anggaran tersebut beserta jumlahnya:
    </span>
    <ol>
    <?php foreach($list_unmatch as $klu => $v_list_unmatch):?>
        <?php foreach($v_list_unmatch as $kvlu => $vlu):?>
            <li>
            <?php
                $crne = '';
                if ( $vlu['total'] && $vlu['total'] != '-' ) {
                    $crne = ' (No. bukti ' . $vlu['cash_receipt_no'] . ' Tgl ' .
                    $time->format('d/m/Y', $vlu['transaction_date']) . ')';
                }
                echo $activities[$klu] . ' &mdash; ' . $kvlu . ' : ' . number_format($vlu['total'],2,'.',',') . $crne;
            ?>
            </li>
        <?php endforeach;?>
    <?php endforeach;?>
    </ol>
    <br />
<?php endif;?>
<table>
    <thead>
        <tr>
            <th rowspan="2" class="center">KODE</th>
            <th rowspan="2" class="center">KEGIATAN/SUB KEGIATAN/JENIS BELANJA/RINCIAN BELANJA</th>
            <th colspan="3" class="center">PERHITUNGAN TAHUN <?php echo $year;?></th>
            <th rowspan="2" class="center">SUMBER DANA</th>
            <th rowspan="2" class="center">Realisasi</th>
            <th rowspan="2" class="center">%</th>
            <th rowspan="2" class="center">Sisa</th>
        </tr>
        <tr>
            <th class="center">VOLUME</th>
            <th class="center">HARGA SATUAN</th>
            <th class="center">JUMLAH BIAYA</th>
        </tr>
    </thead>
    <tbody>
        <?php if ( $total && !empty($records) ): ?>
            <tr>
                <td><strong>10.06.01</strong></td>
                <td><strong>PROGRAM PENDIDIKAN TINGGI</strong></td>
                <td></td>
                <td></td>
                <td class="numeric">
                    <strong>
                        <?php echo number_format($total,2,'.',',');?>
                    </strong>
                </td>
                <td>&nbsp;</td>
                <td>
                    <strong>
                        <?php echo number_format($total_realization,2,'.',',');?>
                    </strong>
                </td>
                <td>
                    <strong>
                        <?php echo number_format($total_percent,2,'.',',') . '&nbsp;%';?>
                    </strong>
                </td>
                <td>
                    <strong>
                        <?php echo number_format($total_remain,2,'.',',');?>
                    </strong>
                </td>
            </tr>
        <?php endif; ?>
        <?php foreach ($records as $r): ?>
            <tr>
                <td nowrap="nowrap">
                    <?php
                        echo $r['pad'];
                        echo '<div class="afterPad">' . 
                             '<strong>' . $r['code'] . '</strong>' .
                             '</div>';
                    ?>
                </td>
                <td>
                    <?php 
                        echo '<strong>' . $r['name'] . '</strong>';
                    ?>
                </td>
                <td>
                    <?php 
                        echo '';
                    ?>
                </td>
                <td>
                    <?php
                        echo '';
                    ?>
                </td>
                <td class="numeric">
                    <?php
                        if ( isset($r['total']) ) {
                            echo '<strong>' . number_format($r['total'],2,'.',',') .
                                 '</strong>';
                        }
                    ?>
                </td>
                <td>
                    <?php
                        if ( isset($r['fund_source']) ) {
                            echo $r['fund_source'];
                        }
                    ?>
                </td>
                <td>
                    <?php
                        if ( isset($r['realization']) ) {
                            echo number_format($r['realization'],2,'.',',');
                        }
                    ?>
                </td>
                <td></td>
                <td></td>
            </tr>
            <?php // check if current row has children ?>
            <?php if ( isset($r['children']) && !empty($r['children']) ): ?>
                <?php foreach ($r['children'] as $_r): ?>
                <tr>
                    <td nowrap="nowrap">
                        <?php
                            echo $r['pad'];
                            echo '<div class="afterPad">&nbsp;&nbsp;' . 
                                 $_r['code'] .
                                 '</div>';
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo $_r['name'];
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo $_r['volume'];
                        ?>
                    </td>
                    <td class="numeric">
                        <?php
                            echo $_r['unit_amount'];
                        ?>
                    </td>
                    <td class="numeric">
                        <?php 
                            echo $_r['total'];
                        ?>
                    </td>
                    <td>
                    <?php
                        if ( isset($_r['fund_source']) ) {
                            echo $_r['fund_source'];
                        }
                    ?>
                    </td>
                    <td>
                    <?php
                        if ( isset($_r['realization']) ) {
                            echo number_format($_r['realization'],2,'.',',');
                        }
                    ?>
                    </td>
                    <td>
                    <?php
                        if ( isset($_r['percent']) ) {
                            echo number_format($_r['percent'],2,'.',',') . '&nbsp;%';
                        }
                    ?>
                    </td>
                    <td>
                    <?php
                        if ( isset($_r['remain']) ) {
                            echo number_format($_r['remain'],2,'.',',');
                        }
                    ?>
                    </td>
                </tr>
                <?php endforeach;?>
            <?php endif;?>
        <?php endforeach; ?>
    </tbody>
</table>