<h1>Komposisi Anggaran <?php echo $year;?></h1>
<br />
<center>
<table>
    <thead>
        <tr>
            <th>&nbsp;</th>
            <th>Alokasi</th>
            <th>Alokasi Anggaran</th>
            <th>Realisasi</th>
            <th>%</th>
            <th>Sisa</th>
        </tr>
    </thead>
    <tbody>
        <?php for ($i = 1; $i <= 3; $i++) { ?>
        <tr>
            <td><strong><?php echo $total[$i]['label'];?></strong></td>
            <td>&nbsp;</td>
            <td class="numeric">
            <strong>
            <?php echo number_format($total[$i]['values'],2,'.',',');?>
            </strong>
            </td>
            <td class="numeric">
            <strong>
            <?php echo $totalR[$i] ? number_format($totalR[$i],2,'.',',') : '';?>
            </strong>
            </td>
            <td class="numeric">
            <strong>
            <?php echo $total[$i]['values'] ? number_format(($totalR[$i]/$total[$i]['values'])*100,2,'.',',') . '&nbsp;%' : '';?>
            </strong>
            </td>
            <td class="numeric">
            <strong>
            <?php echo number_format($total[$i]['values']-$totalR[$i],2,'.',',');?>
            </strong>
            </td>
        </tr>
        <?php foreach (${'t'.$i} as $_k => $_t): ?>
        <tr>
            <td><?php echo '- ' . $_k;?></td>
            <td class="numeric"><?php echo number_format($_t,2,'.',',');?></td>
            <td class="numeric">&nbsp;</td>
            <td class="numeric">
                <?php 
                    echo ${'R'.$i}[$_k] ? number_format(${'R'.$i}[$_k],2,'.',',') : '';
                ?>
            </td>
            <td class="numeric">
                <?php 
                    echo ${'P'.$i}[$_k] ? number_format(${'P'.$i}[$_k],2,'.',',') . '&nbsp;%' : '';
                ?>
            </td>
            <td class="numeric">
                <?php 
                    echo number_format($_t-${'R'.$i}[$_k],2,'.',',');
                ?>
            </td>
        </tr>
        <?php endforeach;?>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <th colspan="2" class="numeric"><strong>Total</strong></th>
            <th class="numeric"><strong><?php echo number_format($totals,2,'.',',');?></strong></th>
            <th class="numeric"><strong><?php echo number_format($totalsR,2,'.',',');?></strong></th>
            <th class="numeric">
                <strong>
                <?php
                    
                    echo $totals ? number_format(($totalsR/$totals)*100,2,'.',',') . '&nbsp;%' : '';
                ?>
                </strong>
            </th>
            <th class="numeric">
                <strong>
                <?php 
                    echo number_format($totals-$totalsR,2,'.',',');
                ?>
                </strong>
            </th>
        </tr>
    </tfoot>
</table>
</center>