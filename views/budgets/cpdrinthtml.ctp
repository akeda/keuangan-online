<h1>Komposisi Anggaran <?php echo $year;?></h1>
<br />
<center>
<table>
    <tbody>
        <tr>
            <td>Mengikat</td>
            <td class="numeric"><?php echo number_format($t['bind'],2,'.',',');?></td>
            <td class="numeric"><?php echo number_format($real['bind'],2,'.',',');?></td>
            <td class="numeric"><?php echo number_format($percent['bind'],2,'.',',');?></td>
            <td class="numeric"><?php echo number_format($remain['bind'],2,'.',',');?></td>
        </tr>
        <tr>
            <td>Tidak Mengikat</td>
            <td class="numeric"><?php echo number_format($t['unbind'],2,'.',',');?></td>
            <td class="numeric"><?php echo number_format($real['unbind'],2,'.',',');?></td>
            <td class="numeric"><?php echo number_format($percent['unbind'],2,'.',',');?></td>
            <td class="numeric"><?php echo number_format($remain['unbind'],2,'.',',');?></td>
        </tr>
        <tr>
            <td>PNBP</td>
            <td class="numeric"><?php echo number_format($t['pnbp'],2,'.',',');?></td>
            <td class="numeric"><?php echo number_format($real['pnbp'],2,'.',',');?></td>
            <td class="numeric"><?php echo number_format($percent['pnbp'],2,'.',',');?></td>
            <td class="numeric"><?php echo number_format($remain['pnbp'],2,'.',',');?></td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
        <th>Total</th>
        <th class="numeric"><?php echo number_format($total,2,'.',',');?></th>
        <th class="numeric"><?php echo number_format($total_r,2,'.',',');?></th>
        <th class="numeric"><?php echo number_format($total_percent,2,'.',',');?></th>
        <th class="numeric"><?php echo number_format($total_remain,2,'.',',');?></th>
        </tr>
    </tfoot>
</table>
</center>