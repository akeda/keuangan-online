<h1>Buku Bank</h1>
<span class="center"><?php echo $monthname . ', ' . $year;?></span>
<center>
<table>
    <thead>
        <tr>
            <th>Tanggal</th>
            <th>Uraian</th>
            <th>Nomor Tanda Bukti</th>
            <th>Penerimaan<br />(Rp.)</th>
            <th>Pengeluaran<br />(Rp.)</th>
            <th>Sisa<br />(Rp.)</th>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($total) && isset($records[0])): ?>
            <tr>
                <td>
                    <?php
                        echo $time->format('j', 
                            $records[0]['JournalBank']['transaction_date']);
                    ?>
                </td>
                <td>Saldo <?php echo $time->format('d/m/Y', $ldaylmonth);?></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    <?php
                        $total['balance'] = $total['balance']*1;
                        echo ($total['balance']) ? number_format($total['balance'],2,'.',',') : '-';
                    ?>
                </td>
            </tr>
        <?php endif;?>
        <?php $tOut = $tIn = 0;?>
        <?php foreach ($records as $record): ?>
            <tr>
                <td>
                    <?php
                        echo $time->format('j', 
                            $record['JournalBank']['transaction_date']);
                    ?>
                </td>
                <td>
                    <?php 
                        echo $record['JournalBank']['description'];
                        if ( !empty($record['JournalBank']['sp2d_no']) ) {
                            echo '<br />No. ' . $record['JournalBank']['sp2d_no'];
                        } else if ( !empty($record['JournalBank']['check_no']) ) {
                            echo '<br />Cek. ' . $record['JournalBank']['check_no'];
                        }
                    ?>
                </td>
                <td>
                    <?php 
                        if ( $record['JournalBank']['debit_amount']*1 ) {
                            echo $record['JournalBank']['sp2d_no'];
                        } else {
                            echo $record['JournalBank']['check_no'];
                        }
                    ?>
                </td>
                <td>
                    <?php
                        if ( ($record['JournalBank']['debit_amount']*1) ) {
                            echo number_format($record['JournalBank']['debit_amount'],2,'.',',');
                            $tIn += $record['JournalBank']['debit_amount'];
                        }
                    ?>
                </td>
                <td>
                    <?php 
                        if ( ($record['JournalBank']['credit_amount']*1) ) {
                            echo number_format($record['JournalBank']['credit_amount'],2,'.',',');
                            $tOut += $record['JournalBank']['credit_amount'];
                        }
                    ?>
                </td>
                <td></td>
            </tr>
        <?php endforeach; ?>
        <?php if (!empty($records)): ?>
            <tr>
            <td>&nbsp;</td>
            <td colspan="2">Total</td>
            <?php 
                if ( isset($total['balance']) ) {
                    $tIn += $total['balance'];
                }
            ?>
            <td><?php echo number_format($tIn,2,'.',',');?></td>
            <td><?php echo number_format($tOut,2,'.',',');?></td>
            <td><?php echo number_format(($tIn - $tOut),2,'.',',');?></td>
            </tr>
        <?php endif;?>
    </tbody>
</table>
</center>