<style type="text/css">
#tax_head {
    width: 500px;
    margin-top: 20px;
}
.double_break {
    height: 1px;
    border-top: 1px solid #000;
    border-bottom: 1px solid #000;
}

#tax_table {
    border-collapse: collapse;
    width: 100%;
}
.sign {
    margin-top: 50px;
    height: auto;
    position: relative;
    width: 100%;
}
.numeric {
    text-align: right;
}
.sign .resp {
    position: absolute;
    top: 0;
    left: 0;
}
.sign .treasurer {
    position: absolute;
    top: 0;
    right: 0;
}
</style>
<table class="noborder separate" id="tax_head">
    <tr>
        <td colspan="3">DAFTAR RINCIAN PENYETORAN PAJAK</td>
    </tr>
    <tr>
        <td width="32%">DEPARTEMENT/LEMBAGA</td>
        <td width="1%">:</td>
        <td width="67%">Pendidikan Nasional (23)</td>
    </tr>
    <tr>
        <td>Unit Organisasi</td>
        <td>:</td>
        <td><strong>Ditjen Pendidikan Tinggi</strong></td>
    </tr>
    <tr>
        <td>L O K A S I</td>
        <td>:</td>
        <td>DKI Jakarta (01)</td>
    </tr>
    <tr>
        <td>KANTOR/SATUAB KERJA</td>
        <td>:</td>
        <td><strong>Politeknik Negeri Media Kreatif (673144)</strong></td>
    </tr>
    <tr>
        <td>ALAMAT</td>
        <td>:</td>
        <td><strong>JL. Srengseng Sawah, Jagakarsa, Jakarta Selatan</strong></td>
    </tr>
    <tr>
        <td>MASA PUNGUTAN</td>
        <td>:</td>
        <td><strong><?php echo $m . ' ' . $y;?></strong></td>
    </tr>
</table>

<table id="tax_table">
<thead>
    <tr>
        <th class="center" width="1%">NO.</th>
        <th class="center">URAIAN</th>
        <th class="center">BUKTI</th>
        <th class="center">PPN. (Rp.)</th>
        <th class="center">PPh. Pasal 21 (Rp.)</th>
        <th class="center">PPh. Pasal 22 (Rp.)</th>
        <th class="center">PPh. Pasal 23 (Rp.)</th>
        <th class="center">Jumlah</th>
    </tr>
</thead>
<?php if (!empty($transaction)):?>
<?php $total = $total_row = $total_ppndn = $total_pph21 = $total_pph22 = $total_pph23 = $no = 0;?>
<tbody>
<?php foreach ($transaction as $k => $t):?>
    <tr>
        <td class="numeric"><?php echo ++$no;?></td>
        <td><?php echo $t['CashReceiver']['name'];?></td>
        <td>
        <?php
            echo $t['Activity']['code'];
            if ( isset($activities[$t['Activity']['parent_id']]) ):
                echo '/' . $activities[$t['Activity']['parent_id']];
            endif;
            echo '/' . $t['ActivityChild']['code'];
        ?>
        </td>
        <td class="numeric">
        <?php
            echo $t['Transaction']['tax_ppndn']*1? number_format($t['Transaction']['tax_ppndn'],2,'.',',') : '';
        ?>
        </td>
        <td class="numeric">
        <?php
            echo $t['Transaction']['tax_pph21']*1? number_format($t['Transaction']['tax_pph21'],2,'.',',') : '';
        ?>
        </td>
        <td class="numeric">
        <?php
            echo $t['Transaction']['tax_pph22']*1? number_format($t['Transaction']['tax_pph22'],2,'.',',') : '';
        ?>
        </td>
        <td class="numeric">
        <?php
            echo $t['Transaction']['tax_pph23']*1? number_format($t['Transaction']['tax_pph23'],2,'.',',') : '';
        ?>
        </td>
        <td class="numeric">
        <?php
            $total_ppndn += $t['Transaction']['tax_ppndn'];
            $total_pph21 += $t['Transaction']['tax_pph21'];
            $total_pph22 += $t['Transaction']['tax_pph22'];
            $total_pph23 += $t['Transaction']['tax_pph23'];
            
            $total_row = $t['Transaction']['tax_ppndn'] +
                         $t['Transaction']['tax_pph21'] +
                         $t['Transaction']['tax_pph22'] +
                         $t['Transaction']['tax_pph23'];
            echo number_format($total_row,2,'.',',');
        ?>
        </td>
    </tr>
    <?php $total += $total_row;?>
<?php endforeach;?>
</tbody>
<tfoot>
    <tr>
        <th colspan="2">&nbsp; JUMLAH</th>
        <th>&nbsp;</th>
        
        <th class="numeric"><?php echo number_format($total_ppndn,2,'.',',');?></th>
        <th class="numeric"><?php echo number_format($total_pph21,2,'.',',');?></th>
        <th class="numeric"><?php echo number_format($total_pph22,2,'.',',');?></th>
        <th class="numeric"><?php echo number_format($total_pph23,2,'.',',');?></th>
        <th class="numeric"><?php echo number_format($total,2,'.',',');?></th>
    </tr>
</tfoot>
<?php endif;?>
</table>

<div class="sign">
<p class="resp">
Mengetahui/Menyetujui<br /><br />
Pejabat Pembuat Komitmen<br /><br /><br /><br /><br /><br />

<strong><?php echo $sign_name;?></strong><br />
NIP.<?php echo $sign_nip;?> &nbsp;&nbsp;&nbsp;&nbsp;
</p>

<p class="treasurer">
Jakarta, <?php echo $_d . ' ' . $_m . ' ' . $_y;?><br /><br />
Bendahara Pengeluaran<br /><br /><br /><br /><br /><br />

<strong><?php echo $treasurer_name;?></strong><br />
NIP.<?php echo $treasurer_nip;?> &nbsp;&nbsp;&nbsp;&nbsp;
</p>
</div>
