<style type="text/css">
h1 {font-size: 16px;}
h2 {font-size: 14px; text-align: center; font-weight: bold}
#sptb_head {
    width: 500px;
    margin-top: 20px;
}
.double_break {
    height: 1px;
    border-top: 1px solid #000;
    border-bottom: 1px solid #000;
}
.info {
    padding: 10px 0;
}
#sptb_table {
    border-collapse: collapse;
    width: 100%;
}
.sign {
    text-align: right;
    padding-right: 50px;
}
.numeric {
    text-align: right;
}
</style>
<h1>SURAT PERNYATAAN TANGGUNG JAWAB BELANJA (SPTB)</h1>
<h2>Nomor : <?php echo $sptb_no;?></h2>
<table class="noborder separate" id="sptb_head">
    <tr>
        <td width="2%">1</td>
        <td width="30%">KANTOR/SATUAN KERJA</td>
        <td width="1%">:</td>
        <td width="67%">POLITEKNIK NEGERI MEDIA KREATIF</td>
    </tr>
    <tr>
        <td>2</td>
        <td>KODE SATUAN KERJA</td>
        <td>:</td>
        <td><strong>023.04.673144</strong></td>
    </tr>
    <tr>
        <td>3</td>
        <td>TGL & NO. DIPA/SKO</td>
        <td>:</td>
        <td><?php echo $dipa_date . ' NO. ' . $dipa_no;?></td>
    </tr>
    <tr>
        <td>4</td>
        <td>SUB KEGIATAN</td>
        <td>:</td>
        <td><strong><?php echo $act;?></strong></td>
    </tr>
    <tr>
        <td>5</td>
        <td>KLASIFIKASI BELANJA</td>
        <td>:</td>
        <td><strong><?php echo $mak_name;?></strong></td>
    </tr>
</table>
<div class="double_break">&nbsp;</div>
<p class="info">
Yang bertanda tangan di bawah ini, Pejabat Pembuat Komitmen Politeknik Negeri Media Kreatif
menyatakan bahwa saya bertanggung jawab penuh atas segala pengeluaran yang telah dibayar
lunas oleh Bendaharawan kepada yang berhak menerima, dengan perincian sebagai berikut :
</p>
<table id="sptb_table">
<thead>
    <tr>
        <th rowspan="2" class="center">NO.</th>
        <th rowspan="2" class="center">MAK</th>
        <th rowspan="2" class="center">PENERIMAAN</th>
        <th rowspan="2" class="center">URAIAN</th>
        <th colspan="2" class="center">NO BUKTI</th>
        <th rowspan="2" class="center">JUMLAH YANG DITERIMA (Rp.)</th>
    </tr>
    <tr>
        <th>Tanggal</th>
        <th>Nomor</th>
    </tr>
</thead>
<?php if (!empty($transaction)):?>
<?php $total = 0;?>
<tbody>
<?php foreach ($transaction as $k => $t):?>
    <tr>
        <td><?php echo $k+1;?></td>
        <td><?php echo $t['ActivityChild']['code'];?></td>
        <td><?php echo $t['CashReceiver']['name'];?></td>
        <td><?php echo $t['Transaction']['description'];?></td>
        <td><?php echo $t['Transaction']['cash_receipt_date'];?></td>
        <td><?php echo $t['Transaction']['cash_receipt_no'];?></td>
        <td class="numeric"><?php echo number_format($t['Transaction']['amount_out'], 2, ',', '.');?></td>
    </tr>
    <?php $total += $t['Transaction']['amount_out'];?>
<?php endforeach;?>
</tbody>
<tfoot>
    <tr>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th class="center" colspan="3">Jumlah</th>
        <th class="numeric"><?php echo number_format($total, 2, ',', '.');?></th>
    </tr>
</tfoot>
<?php endif;?>
</table>
<p class="info">
Bukti-bukti belanja tsb, di atas disimpan sesuai ketentuan yang berlaku di Politeknik Negeri Media Kreatif,
Kementerian Pendidikan Nasional untuk kelengkapan administrasi dan keperluan pemeriksaan aparat pengawasan
Fungsional
</p>
<p class="sign">
Jakarta, <?php echo $sptb_date;?><br /><br />
Pejabat Pembuat Komitmen<br /><br /><br /><br /><br /><br />

<strong><?php echo $sign_name;?></strong><br />
NIP.<?php echo $sign_nip;?> &nbsp;&nbsp;&nbsp;&nbsp;
</p>
