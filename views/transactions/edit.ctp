<?php echo $javascript->codeBlock($ajaxURL);?>
<?php echo $javascript->link('jquery.numeric', false);?>
<?php echo $javascript->link('transactions.js?20101211', false);?>
<?php echo $html->css('transactions', 'stylesheet', array('inline' => false));?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Transaction', array('action' => 'edit/' . $transaction_date,));?>
	<fieldset>
 		<legend><?php __('Edit Transaksi');?></legend>
        <table class="input">
            <tr>
                <td colspan="2">
                    <div id="information">
                        <h2>Tata cara menambah / menyunting data transaksi</h2>
                        <ol>
                            <li>Masukkan <strong>Tanggal Transaksi</strong></li>
                            <li>
                                Semua transaksi yang dimasukkan di bawah berarti
                                transaksi yang terjadi pada tanggal yang dimasukkan di<br />
                                <strong>Tanggal Transaksi (No. 1)</strong>. Satu baris hanya
                                bisa penerimaan atau pengeluaran saja.
                            </li>
                            <li>
                                Isi semua transaksi berupa <strong>pengeluaran</strong> atau <strong>penerimaan</strong>
                                di table yang disediakan, untuk menambah transaksi,<br />
                                klik tombol <strong>+ Tambah Transaksi</strong>
                            </li>
                            <li>
                                Jika ada <strong>pengeluaran dari bank</strong>, masukkan besarnya di kolom
                                <strong>pengeluaran</strong>. Lalu pilih asal pengeluaran dari <strong>Bank</strong>.<br />
                                Jika ada No. cek-nya, masukkan nomor tersebut. Ini akan secara<br />
                                otomatis menyimpan transaksi penerimaan di buku kas tunai dan umum yang berasal dari cek tersebut,
                                jadi<br />tidak perlu lagi memasukkannya sebagai penerimaan kas.
                            </li>
                            <li>
                                Jika ada <strong>penerimaan ke bank</strong>, masukkan besarnya di kolom
                                <strong>penerimaan</strong>. Lalu pilih asal penerimaan dari Bank.<br />
                                Jika ada No. SP2D-nya, masukkan nomor tersebut. Untuk memasukkannya ke kas, Anda perlu transaksi<br />
                                pengeluaran dari Bank dengan cara
                                klik <strong>+ Tambah Transaksi</strong>, dan lakukan langkah no. 4.
                            </li>
                            <li>
                                Untuk <strong>penerimaan ke kas</strong> yang bukan dari bank / cek yang dicairkan, masukkan<br />
                                jumlah penerimaan, lalu pilih asal penerimaan <strong>Kas</strong>. Ini hanya akan tercatat di penerimaan buku kas tunai dan buku kas umum, dan<br />
                                <strong>tidak akan tercatat di buku bank</strong>
                            </li>
                            <li>
                                Untuk <strong>pengeluaran dari kas</strong>, masukkan
                                <strong>jumlah pengeluaran</strong>, lalu pilih asal pengeluaran <strong>Kas</strong>.<br />Pilih Kegiatan, sub kegiatan dan MAK-nya, lalu pilih <strong>Jenis Pajak-nya</strong> &mdash; jika sudah ada SPJ-nya.<br />
                                Jika ada pajaknya, maka akan secara otomatis tercatat di buku kas umum sebagai penerimaan<br />
                                dan di buku pajak.
                            </li>
                            <li>
                                Kolom <strong>total</strong> di sebelah kanan berisi jumlah penerimaan/pengeluaran di kurangi
                                besar pajak,<br />jika <strong>SPJ dipilih opsi sudah</strong> dan ada <strong>tipe pajak yang dipilih</strong>.
                            </li>
                        </ol>
                    </div>
                    <span><a href="#information" id="show_information">Klik disini keterangan untuk tata cara</a></span>
                </td>
            </tr>
            <tr>
                <td class="label-required" colspan="2">
                    <?php echo __('Tanggal Transaksi', true) . '&nbsp;';?>
                    <?php
                        echo $form->input('transaction_date', array(
                            'div' => false, 'label' => false,
                            'class' => 'inputText',
                            'selected' => $transaction_date
                        ));
                    ?>
                </td>
            </td>

            <tr>
                <td colspan="2">
                    <table id="transactions" class="transactions">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>Kegiatan / Sub Kegiatan / MAK / Unit Kerja</th>
                                <th>Rincian</th>
                                <th>Pengeluaran</th>
                                <th>Penerimaan</th>
                                <th>SPJ</th>
                                <th>Pajak</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if ( isset($transactions) ): ?>
                        <?php
                            /**
                             * we need to unset this from Transaction
                             */
                            unset($transactions['Transaction']['transaction_date']);
                            unset($transactions['Transaction']['created_by']);

                            /**
                             * When first time saving is failed,
                             * we still have it set
                             */
                            $total_transaction = 0;
                            $total_in = 0;
                            $total_out = 0;
                            foreach ($transactions['Transaction'] as $key => $transaction):
                                echo '<tr class="transaction" id="r'.$key.'">';
                                echo '<td><input type="checkbox" ' .
                                     'name="data[Transaction]['.$key.'][id]" ' .
                                     'class="cb_transaction" class="inputText" /></td>';

                                echo '<td><span class="label_trans">Kegiatan:</span>';
                                echo $form->select(
                                            'parent_activity_id', $activities,
                                            $transaction['parent_activity_id'],
                                            array(
                                                'id' => null,
                                                'class' => 'parent_activity_id ajax_select select',
                                                'rel' => '#TransactionActivityId' . $key,
                                                'ref' => 'parent_id',
                                                'name' => 'data[Transaction]['.$key.'][parent_activity_id]'
                                            ),
                                            ''
                                        );
                                echo '<span class="label_trans">Sub Kegiatan:</span>';
                                echo $form->select(
                                            'activity_id', $subactivities,
                                            $transaction['activity_id'], array(
                                                'id' => 'TransactionActivityId' . $key,
                                                'class' => 'activity_id select',
                                                'model' => 'Activity',
                                                'field' => 'name',
                                                'name' => 'data[Transaction]['.$key.'][activity_id]'
                                            ),
                                            ''
                                        );
                                echo '<span class="label_trans">MAK:</span>';
                                echo $form->select(
                                            'activity_child_id', $activity_children,
                                            $transaction['activity_child_id'], array(
                                                'id' => null,
                                                'class' => 'activity_child_id select',
                                                'name' => 'data[Transaction]['.$key.'][activity_child_id]'
                                            ),
                                            ''
                                        );
                                echo '<span class="label_trans">Unit Kerja:</span>';
                                echo $form->select(
                                            'unit_code_id', $unit_codes,
                                            $transaction['unit_code_id'], array(
                                                'id' => null,
                                                'class' => 'unit_code_id select',
                                                'name' => 'data[Transaction]['.$key.'][unit_code_id]'
                                            ),
                                            ''
                                        );
                                echo '</td>';

                                echo '<td>';
                                echo $form->input('description',
                                    array(
                                        'id' => null,
                                        'name' => 'data[Transaction]['.$key.'][description]',
                                        'class' => 'inputText description',
                                        'type' => 'textarea', 'div' => false, 'label' => false,
                                        'value' => $transaction['description']
                                    )
                                );
                                echo '<span class="label_trans" style="color: black"><strong>Atas nama:</strong></span>';
                                echo $form->select('cash_receiver_id',
                                        $cash_receivers,
                                        $transaction['cash_receiver_id'],
                                        array(
                                            'id' => null,
                                            'name' => 'data[Transaction]['.$key.'][cash_receiver_id]',
                                            'class' => 'inputText cash_receiver_id'
                                        )
                                );
                                echo '</td>';

                                echo '<td><span class="label_trans">Besar pengeluaran:</span>';
                                echo $form->input('amount_out', array(
                                            'div' => false, 'label' => false,
                                            'id' => null, 'class' => 'inputText amount_out numeric select',
                                            'name' => 'data[Transaction]['.$key.'][amount_out]',
                                            'value' => number_format($transaction['amount_out'],2,'.',',')
                                        ));
                                echo ($form->isFieldError('Transaction.'.$key.'.amount_out')) ? $form->error('Transaction.'.$key.'.amount_out') : '';
                                echo '<span class="label_trans">Asal pengeluaran:</span>';
                                echo '<input class="r_amount_out" id="TransactionRAmountOutBank'.$key.'" '.
                                     'name="data[Transaction]['.$key.'][r_amount_out]" type="radio" ' .
                                     'rel="#TransactionEAmountOutBank'.$key.'"' .
                                        ($transaction['check_no'] ? ' checked="checked"' : '')
                                     . ' />' .
                                     '<label for="TransactionRAmountOutBank'.$key.'" class="rlabel">Bank</label>' .
                                     '<input class="r_amount_out" id="TransactionRAmountOutKas_'.$key.'"' .
                                     'name="data[Transaction]['.$key.'][r_amount_out]" type="radio" ' .
                                     'rel="#TransactionEAmountOutKas_'.$key.'"' .
                                        ($transaction['cash_receipt_no'] ? ' checked="checked"' : '')
                                     . ' />' .
                                     '<label for="TransactionRAmountOutKas_'.$key.'" class="rlabel">Kas</label>';

                                    echo '<div class="evidence_no" id="TransactionEAmountOutBank'.$key.'"' .
                                            ($transaction['check_no'] ? ' style="display: block;"' : '') .
                                         '>';
                                    echo '<span class="label_trans">No Cek:</span>';
                                            echo $form->input('check_no', array(
                                                'div' => false, 'label' => false,
                                                'id' => null, 'class' => 'inputText check_no',
                                                'name' => 'data[Transaction]['.$key.'][check_no]',
                                                'value' => $transaction['check_no']
                                            ));
                                     echo '</div>';

                                     echo '<div class="evidence_no" id="TransactionEAmountOutKas_'.$key.'"' .
                                            ($transaction['cash_receipt_no'] ? ' style="display: block;"' : '') .
                                          '>';
                                     echo '<span class="label_trans">No. Tanda bukti kas:</span>';
                                            echo $form->input('cash_receipt_no', array(
                                                'div' => false, 'label' => false,
                                                'id' => null, 'class' => 'inputText cash_receipt_no',
                                                'name' => 'data[Transaction]['.$key.'][cash_receipt_no]',
                                                'value' => $transaction['cash_receipt_no']
                                            ));
                                    echo '<span class="label">Jika tidak diisi, sistem akan membuatkannya</span>';
                                    echo ($form->isFieldError('Transaction.'.$key.'.cash_receipt_no')) ? $form->error('Transaction.'.$key.'.cash_receipt_no') : '';

                                    echo '<span class="label_trans">Tgl. Tanda bukti kas:</span>';
                                            echo $form->month('cash_receipt_date', $transaction['cash_receipt_date'], array(
                                                'id' => null, 'class' => 'inputText cash_receipt_date',
                                                'name' => 'data[Transaction]['.$key.'][cash_receipt_date][month]'
                                            )) . '-';
                                            echo $form->day('cash_receipt_date', $transaction['cash_receipt_date'], array(
                                                'id' => null, 'class' => 'inputText cash_receipt_date',
                                                'name' => 'data[Transaction]['.$key.'][cash_receipt_date][day]'
                                            )) . '-';
                                            echo $form->year('cash_receipt_date', '2008', date('Y'), $transaction['cash_receipt_date'], array(
                                                'id' => null, 'class' => 'inputText cash_receipt_date',
                                                'name' => 'data[Transaction]['.$key.'][cash_receipt_date][year]'
                                            ));
                                    echo ($form->isFieldError('Transaction.'.$key.'.cash_receipt_date')) ? $form->error('Transaction.'.$key.'.cash_receipt_date') : '';
                                    echo '</div>';
                                echo '</td>';

                                echo '<td><span class="label_trans">Besar penerimaan:</span>';
                                echo $form->input('amount_in', array(
                                            'div' => false, 'label' => false,
                                            'id' => null, 'class' => 'inputText amount_in numeric select',
                                            'name' => 'data[Transaction]['.$key.'][amount_in]',
                                            'value' => number_format($transaction['amount_in'],2,'.',',')
                                        ));
                                echo ($form->isFieldError('Transaction.'.$key.'.amount_in')) ? $form->error('Transaction.'.$key.'.amount_in') : '';
                                echo ($form->isFieldError('amount_in')) ? $form->error('amount_in') : '';
                                echo '<span class="label_trans">Asal penerimaan:</span>';
                                echo '<input class="r_amount_in" id="TransactionRAmountInBank'.$key.'" ' .
                                     'name="data[Transaction]['.$key.'][r_amount_in]" type="radio" ' .
                                     'rel="#TransactionEAmountInBank'.$key.'"' .
                                        ($transaction['sp2d_no'] ? ' checked="checked"' : '')
                                     . ' />' .
                                     '<label for="TransactionRAmountInBank'.$key.'" class="rlabel">Bank</label>' .
                                     '<input class="r_amount_in" id="TransactionRAmountInKas_'.$key.'" ' .
                                     'name="data[Transaction]['.$key.'][r_amount_in]" type="radio" ' .
                                     'rel="#TransactionEAmountInKas_'.$key.'"' .
                                        ($transaction['cash_receipt_no_in'] ? ' checked="checked"' : '')
                                     . ' />' .
                                     '<label for="TransactionRAmountInKas_'.$key.'" class="rlabel">Kas</label>';

                                    echo '<div class="evidence_no" id="TransactionEAmountInBank'.$key.'"' .
                                            ($transaction['sp2d_no'] ? ' style="display: block;"' : '') .
                                         '>';
                                    echo '<span class="label_trans">No SP2D:</span>';
                                            echo $form->input('sp2d_no', array(
                                                'div' => false, 'label' => false,
                                                'id' => null, 'class' => 'inputText sp2d_no',
                                                'name' => 'data[Transaction]['.$key.'][sp2d_no]',
                                                'value' => $transaction['sp2d_no']
                                            ));
                                    echo '</div>';

                                    echo '<div class="evidence_no" id="TransactionEAmountInKas_'.$key.'"' .
                                            ($transaction['cash_receipt_no_in'] ? ' style="display: block;"' : '') .
                                         '>';
                                    echo '<span class="label_trans">No. Tanda bukti kas:</span>';
                                            echo $form->input('cash_receipt_no_in', array(
                                                'div' => false, 'label' => false,
                                                'id' => null, 'class' => 'inputText cash_receipt_no_in',
                                                'name' => 'data[Transaction]['.$key.'][cash_receipt_no_in]',
                                                'value' => $transaction['cash_receipt_no_in']
                                            ));
                                    echo '<span class="label">Jika tidak diisi, sistem akan membuatkannya</span>';
                                    echo ($form->isFieldError('Transaction.'.$key.'.cash_receipt_no_in')) ? $form->error('Transaction.'.$key.'.cash_receipt_no_in') : '';
                                    echo '</div>';
                                    echo '<span class="label_trans">Besar potongan:</span>';
                                    echo $form->input('reduction', array(
                                        'div' => false, 'label' => false,
                                        'id' => null, 'class' => 'inputText reduction numeric select',
                                        'value' => number_format($transaction['reduction'],2,'.',','),
                                        'name' => 'data[Transaction]['.$key.'][reduction]'
                                    ));
                                echo '</td>';

                                echo '<td>';
                                echo $form->select('spj',
                                        array(
                                            0 => 'Belum',
                                            1 => 'Sudah'
                                        ),
                                        $transaction['spj'], array(
                                            'id' => null,
                                            'class' => 'inputText spj',
                                            'name' => 'data[Transaction]['.$key.'][spj]'
                                        ),
                                        false
                                    );
                                echo '</td>';

                                echo '<td>';
                                echo '<span class="label_trans">PPN-DN:</span>';
                                echo $form->input('tax_ppndn', array(
                                    'div' => false, 'label' => false,
                                    'id' => null, 'class' => 'inputText tax_ppndn tax numeric select',
                                    'name' => 'data[Transaction]['.$key.'][tax_ppndn]',
                                    'value' => number_format($transaction['tax_ppndn'],2,'.',',')
                                ));
                                echo '<span class="label_trans">PPH-21:</span>';
                                echo $form->input('tax_pph21', array(
                                    'div' => false, 'label' => false,
                                    'id' => null, 'class' => 'inputText tax_pph21 tax numeric select',
                                    'name' => 'data[Transaction]['.$key.'][tax_pph21]',
                                    'value' => number_format($transaction['tax_pph21'],2,'.',',')
                                ));
                                echo '<span class="label_trans">PPH-22:</span>';
                                echo $form->input('tax_pph22', array(
                                    'div' => false, 'label' => false,
                                    'id' => null, 'class' => 'inputText tax_pph22 tax numeric select',
                                    'name' => 'data[Transaction]['.$key.'][tax_pph22]',
                                    'value' => number_format($transaction['tax_pph22'],2,'.',',')
                                ));
                                echo '<span class="label_trans">PPH-23:</span>';
                                echo $form->input('tax_pph23', array(
                                    'div' => false, 'label' => false,
                                    'id' => null, 'class' => 'inputText tax_pph23 tax numeric select',
                                    'name' => 'data[Transaction]['.$key.'][tax_pph23]',
                                    'value' => number_format($transaction['tax_pph23'],2,'.',',')
                                ));
                                echo '<span class="label_trans">LAIN-LAIN:</span>';
                                echo $form->input('tax_other', array(
                                    'div' => false, 'label' => false,
                                    'id' => null, 'class' => 'inputText tax_other tax numeric select',
                                    'name' => 'data[Transaction]['.$key.'][tax_other]',
                                    'value' => number_format($transaction['tax_other'],2,'.',',')
                                ));
                                echo '</td>';

                                $transaction['amount_out'] = str_replace(',', '', $transaction['amount_out'])*1;
                                $transaction['amount_in'] = str_replace(',', '', $transaction['amount_in'])*1;
                                $transaction['reduction'] = str_replace(',', '', $transaction['reduction'])*1;
                                $transaction['tax_ppndn'] = str_replace(',', '', $transaction['tax_ppndn'])*1;
                                $transaction['tax_pph21'] = str_replace(',', '', $transaction['tax_pph21'])*1;
                                $transaction['tax_pph22'] = str_replace(',', '', $transaction['tax_pph22'])*1;
                                $transaction['tax_pph23'] = str_replace(',', '', $transaction['tax_pph23'])*1;
                                $transaction['tax_other'] = str_replace(',', '', $transaction['tax_other'])*1;
                                $total = 0;
                                $rel = 'out';
                                // decreased by tax
                                $tax = 0;
                                $tax = $transaction['tax_ppndn']+$transaction['tax_pph21']+
                                       $transaction['tax_pph22']+$transaction['tax_pph23']+
                                       $transaction['tax_other'];
                                $reduction = 0;
                                $reduction = $transaction['reduction'];

                                if ( $transaction['amount_out'] ) {
                                    $total = $transaction['amount_out']-$tax;
                                    $total_out += $transaction['amount_out'];
                                } else if ( $transaction['amount_in'] ) {
                                    $total = $transaction['amount_in']-($tax+$reduction);
                                    $rel = 'in';
                                    $total_in += $transaction['amount_in'];
                                }

                                echo '<td><span class="total" rel="'.$rel.'">';
                                echo number_format($total,2,'.',',');
                                echo '</span></td>';
                                echo '</tr>';
                            endforeach; // eof iterate transactions
                        ?>
                        <?php else: ?>
                            <tr class="transaction" id="r0">
                                <td>
                                    <input type="checkbox" name="data[Transaction][0][id]" class="cb_transaction" class="inputText" />
                                </td>
                                <td>
                                    <span class="label_trans">Kegiatan:</span>
                                    <?php
                                        echo $form->select(
                                            'parent_activity_id', $activities,
                                            null, array(
                                                'id' => null,
                                                'class' => 'parent_activity_id ajax_select select',
                                                'rel' => '#TransactionActivityId0',
                                                'ref' => 'parent_id',
                                                'name' => 'data[Transaction][0][parent_activity_id]'
                                            ),
                                            ''
                                        );
                                    ?>
                                    <span class="label_trans">Sub Kegiatan:</span>
                                    <?php
                                        echo $form->select(
                                            'activity_id', $subactivities,
                                            null, array(
                                                'id' => 'TransactionActivityId0',
                                                'class' => 'activity_id select',
                                                'model' => 'Activity',
                                                'field' => 'name',
                                                'name' => 'data[Transaction][0][activity_id]'
                                            ),
                                            ''
                                        );
                                    ?>
                                    <span class="label_trans">MAK:</span>
                                    <?php
                                        echo $form->select(
                                            'activity_child_id', $activity_children,
                                            null, array(
                                                'id' => null,
                                                'class' => 'activity_child_id select',
                                                'name' => 'data[Transaction][0][activity_child_id]'
                                            ),
                                            ''
                                        );
                                    ?>
                                    <span class="label_trans">Unit Kerja:</span>
                                    <?php
                                        echo $form->select(
                                            'unit_code_id', $unit_codes,
                                            null, array(
                                                'id' => null,
                                                'class' => 'unit_code_id select',
                                                'name' => 'data[Transaction][0][unit_code_id]'
                                            ),
                                            ''
                                        );
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        echo $form->input('description',
                                            array(
                                                'id' => null,
                                                'name' => 'data[Transaction][0][description]',
                                                'class' => 'inputText description',
                                                'type' => 'textarea', 'div' => false, 'label' => false
                                            )
                                        );
                                        echo '<span class="label_trans" style="color: black"><strong>Atas nama:</strong></span>';
                                        echo $form->select('cash_receiver_id',
                                            $cash_receivers, null,
                                            array(
                                                'id' => null,
                                                'name' => 'data[Transaction][0][cash_receiver_id]',
                                                'class' => 'inputText cash_receiver_id'
                                            )
                                        );
                                    ?>
                                </td>
                                <td>
                                    <span class="label_trans">Besar pengeluaran:</span>
                                    <?php
                                        echo $form->input('amount_out', array(
                                            'div' => false, 'label' => false,
                                            'id' => null, 'class' => 'inputText amount_out numeric select',
                                            'name' => 'data[Transaction][0][amount_out]'
                                        ));
                                    ?>
                                    <span class="label_trans">Asal pengeluaran:</span>
                                    <input class="r_amount_out" id="TransactionRAmountOutBank0" name="data[Transaction][0][r_amount_out]" type="radio" rel="#TransactionEAmountOutBank0" />
                                    <label for="TransactionRAmountOutBank0" class="rlabel">Bank</label>
                                    <input class="r_amount_out" id="TransactionRAmountOutKas_0" name="data[Transaction][0][r_amount_out]" type="radio" rel="#TransactionEAmountOutKas_0" />
                                    <label for="TransactionRAmountOutKas_0" class="rlabel">Kas</label>

                                    <div class="evidence_no" id="TransactionEAmountOutBank0">
                                        <span class="label_trans">No Cek:</span>
                                        <?php
                                            echo $form->input('check_no', array(
                                                'div' => false, 'label' => false,
                                                'id' => null, 'class' => 'inputText check_no',
                                                'name' => 'data[Transaction][0][check_no]'
                                            ));
                                        ?>
                                    </div>

                                    <div class="evidence_no" id="TransactionEAmountOutKas_0">
                                        <span class="label_trans">No. Tanda bukti kas:</span>
                                        <?php
                                            echo $form->input('cash_receipt_no', array(
                                                'div' => false, 'label' => false,
                                                'id' => null, 'class' => 'inputText cash_receipt_no',
                                                'name' => 'data[Transaction][0][cash_receipt_no]'
                                            ));
                                        ?>
                                        <span class="label">Jika tidak diisi, sistem akan membuatkannya</span>
                                        <br />
                                        <span class="label_trans">Tgl. Tanda bukti kas:</span>
                                        <?php
                                            echo $form->input('cash_receipt_date', array(
                                                'div' => false, 'label' => false,
                                                'id' => null, 'class' => 'inputText cash_receipt_date',
                                                'name' => 'data[Transaction][0][cash_receipt_date][month]',
                                                'type' => 'date', 'dateFormat' => 'M'
                                            )) . '-';
                                            echo $form->input('cash_receipt_date', array(
                                                'div' => false, 'label' => false,
                                                'id' => null, 'class' => 'inputText cash_receipt_date',
                                                'name' => 'data[Transaction][0][cash_receipt_date][day]',
                                                'type' => 'date', 'dateFormat' => 'D'
                                            )) . '-';
                                            echo $form->input('cash_receipt_date', array(
                                                'div' => false, 'label' => false,
                                                'id' => null, 'class' => 'inputText cash_receipt_date',
                                                'name' => 'data[Transaction][0][cash_receipt_date][year]',
                                                'type' => 'date', 'dateFormat' => 'Y'
                                            )) . '-';
                                        ?>
                                    </div>
                                </td>
                                <td>
                                    <span class="label_trans">Besar penerimaan:</span>
                                    <?php
                                        echo $form->input('amount_in', array(
                                            'div' => false, 'label' => false,
                                            'id' => null, 'class' => 'inputText amount_in numeric select',
                                            'name' => 'data[Transaction][0][amount_in]'
                                        ));
                                    ?>
                                    <span class="label_trans">Asal penerimaan:</span>
                                    <input class="r_amount_in" id="TransactionRAmountInBank0" name="data[Transaction][0][r_amount_in]" type="radio" rel="#TransactionEAmountInBank0" />
                                    <label for="TransactionRAmountInBank0" class="rlabel">Bank</label>
                                    <input class="r_amount_in" id="TransactionRAmountInKas_0" name="data[Transaction][0][r_amount_in]" type="radio" rel="#TransactionEAmountInKas_0" />
                                    <label for="TransactionRAmountInKas_0" class="rlabel">Kas</label>

                                    <div class="evidence_no" id="TransactionEAmountInBank0">
                                        <span class="label_trans">No SP2D:</span>
                                        <?php
                                            echo $form->input('sp2d_no', array(
                                                'div' => false, 'label' => false,
                                                'id' => null, 'class' => 'inputText sp2d_no',
                                                'name' => 'data[Transaction][0][sp2d_no]'
                                            ));
                                        ?>
                                    </div>

                                    <div class="evidence_no" id="TransactionEAmountInKas_0">
                                        <span class="label_trans">No. Tanda bukti kas:</span>
                                        <?php
                                            echo $form->input('cash_receipt_no_in', array(
                                                'div' => false, 'label' => false,
                                                'id' => null, 'class' => 'inputText cash_receipt_no_in',
                                                'name' => 'data[Transaction][0][cash_receipt_no_in]'
                                            ));
                                        ?>
                                        <span class="label">Jika tidak diisi, sistem akan membuatkannya</span>
                                    </div>

                                    <span class="label_trans">Besar potongan:</span>
                                    <?php
                                        echo $form->input('reduction', array(
                                            'div' => false, 'label' => false,
                                            'id' => null, 'class' => 'inputText reduction numeric select',
                                            'name' => 'data[Transaction][0][reduction]'
                                        ));
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        echo $form->select('spj',
                                            array(
                                                0 => 'Belum',
                                                1 => 'Sudah'
                                            ),
                                            null, array(
                                                'id' => null,
                                                'class' => 'inputText spj',
                                                'name' => 'data[Transaction][0][spj]'
                                            ),
                                            false
                                        );
                                    ?>
                                </td>
                                <td>
                                    <span class="label_trans">PPN-DN:</span>
                                    <?php
                                        echo $form->input('tax_ppndn', array(
                                            'div' => false, 'label' => false,
                                            'id' => null, 'class' => 'inputText tax_ppndn tax numeric select',
                                            'name' => 'data[Transaction][0][tax_ppndn]'
                                        ));
                                    ?>
                                    <span class="label_trans">PPH-21:</span>
                                    <?php
                                        echo $form->input('tax_pph21', array(
                                            'div' => false, 'label' => false,
                                            'id' => null, 'class' => 'inputText tax_pph21 tax numeric select',
                                            'name' => 'data[Transaction][0][tax_pph21]'
                                        ));
                                    ?>
                                    <span class="label_trans">PPH-22:</span>
                                    <?php
                                        echo $form->input('tax_pph22', array(
                                            'div' => false, 'label' => false,
                                            'id' => null, 'class' => 'inputText tax_pph22 tax numeric select',
                                            'name' => 'data[Transaction][0][tax_pph22]'
                                        ));
                                    ?>
                                    <span class="label_trans">PPH-23:</span>
                                    <?php
                                        echo $form->input('tax_pph23', array(
                                            'div' => false, 'label' => false,
                                            'id' => null, 'class' => 'inputText tax_pph23 tax numeric select',
                                            'name' => 'data[Transaction][0][tax_pph23]'
                                        ));
                                    ?>
                                    <span class="label_trans">LAIN-LAIN:</span>
                                    <?php
                                        echo $form->input('tax_other', array(
                                            'div' => false, 'label' => false,
                                            'id' => null, 'class' => 'inputText tax_other tax numeric select',
                                            'name' => 'data[Transaction][0][tax_other]'
                                        ));
                                    ?>
                                </td>
                                <td><span class="total"></span></td>
                            </tr>
                        <?php endif;?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="3">
                                    <span class="up"></span>
                                    <input type="button" name="add_row" id="add_row" value="+ Tambah Transaksi" /> &nbsp; atau &nbsp;
                                    <input type="button" name="del_row" id="del_row" value="- Hapus Transaksi" />
                                </th>
                                <th>
                                    <span id="total_amount_out">
                                    <?php
                                        if (isset($total_out)):
                                            echo number_format($total_out,2,'.',',');
                                        endif
                                    ?>
                                    </span>
                                </th>
                                <th>
                                    <span id="total_amount_in">
                                    <?php
                                        if (isset($total_in)):
                                            echo number_format($total_in,2,'.',',');
                                        endif
                                    ?>
                                    </span>
                                </th>
                                <th colspan="2" class="numeric">
                                    <span class="total_label">Out of balance</span>
                                </th>
                                <th class="numeric">
                                    <span id="total_transaction">
                                    <?php
                                        if (isset($total_transaction)):
                                            $total_transaction = $total_in - $total_out;
                                            echo number_format($total_transaction,2,'.',',');
                                        endif
                                    ?>
                                    </span>
                                    <span class="up"></span>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Simpan Transaksi ini', array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
