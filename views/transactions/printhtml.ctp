<h1>Buku Kas Umum</h1>
<table class="noborder separate">
    <tr>
        <td class="left bold">PENERIMAAN</td>
        <td class="center bold"><span>BULAN : <?php echo $monthname;?> TAHUN : <?php echo $year;?></span></td>
        <td class="right bold">PENGELUARAN</td>
    </tr>
</table>

<table class="separate">
    <thead>
        <tr>
            <th>TANGGAL</th>
            <th>URAIAN</th>
            <th>NOMOR TANDA BUKTI KAS</th>
            <th>KODE MATA ANGGARAN</th>
            <th>JUMLAH<br />(Rp.)</th>
            <th width="1%" class="whiteborder">&nbsp;</th>
            <th>TANGGAL</th>
            <th>URAIAN</th>
            <th>NOMOR TANDA BUKTI KAS</th>
            <th>KODE MATA ANGGARAN</th>
            <th>JUMLAH<br />(Rp.)</th>
        </tr>
    </thead>
    <tbody>
        <?php $tIn = $tOut = 0;?>
        <?php if (isset($balanceinquiry) && isset($rIn[0])): ?>
            <tr>
                <td>
                    <?php
                        echo $time->format('j', 
                            $rIn[0]['transaction_date']);
                    ?>
                </td>
                <td>Saldo <?php echo $time->format('d/m/Y', $ldaylmonth);?></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    <?php
                        echo number_format($balanceinquiry,2,'.',',');
                    ?>
                </td>
                <td width="1%" class="whiteborder">&nbsp;</td>
                <!-- out -->
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        <?php endif;?>
        <?php foreach ($rIn as $k => $_rIn): ?>
            <tr>
                <!-- in -->
                <td>
                    <?php
                        if ( !empty($_rIn['transaction_date']) ) {
                            echo $time->format('j', 
                                $_rIn['transaction_date']);
                        }
                    ?>
                </td>
                <td>
                    <?php 
                        echo $_rIn['description'];
                    ?>
                </td>
                <td>
                    <?php
                        if (isset($_rIn['cash_receipt_no_in'])) {
                            echo $_rIn['cash_receipt_no_in'];
                        }
                    ?>
                </td>
                <td>
                    <?php
                        if (isset($_rIn['mak_code'])) {
                            echo $_rIn['mak_code'];
                        } else {
                            echo '&nbsp;';
                        }
                    ?>
                </td>
                <td>
                    <?php
                        if ( $_rIn['debit_amount'] ) {
                            $tIn += $_rIn['debit_amount'];
                            echo number_format($_rIn['debit_amount'],2,'.',',');
                        }
                    ?>
                </td>
                <td width="2%" class="whiteborder">&nbsp;</td>
                <td>
                    <?php
                        if ( !empty($rOut[$k]['transaction_date']) ) {
                            echo $time->format('j', 
                                $rOut[$k]['transaction_date']);
                        }
                    ?>
                </td>
                <td>
                    <?php 
                        echo $rOut[$k]['description'];
                    ?>
                </td>
                <td>
                    <?php
                        if ( isset($rOut[$k]['cash_receipt_no']) ) {
                            echo $rOut[$k]['cash_receipt_no'];
                        }
                    ?>
                </td>
                <td>
                    <?php
                        if (isset($rOut[$k]['mak_code'])) {
                            echo $rOut[$k]['mak_code'];
                        } else {
                            echo '&nbsp;';
                        }
                    ?>
                </td>
                <td>
                    <?php
                        if ( $rOut[$k]['credit_amount'] ) {
                            $tOut += $rOut[$k]['credit_amount'];
                            echo number_format($rOut[$k]['credit_amount'],2,'.',',');
                        }
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
        <?php if (!empty($rIn) || !empty($rOut)): ?>
            <tr>
                <td colspan="4">Jumlah yang dipindahkan</td>
                <td><?php echo number_format($tIn+$balanceinquiry,2,'.',',');?></td>
                <td class="whiteborder"></td>
                <td colspan="4">Jumlah yang dipindahkan</td>
                <td><?php echo number_format($tOut,2,'.',',');?></td>
            </tr>
        <?php endif;?>
    </tbody>
</table>
