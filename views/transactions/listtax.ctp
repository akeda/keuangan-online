<?php echo $javascript->codeBlock($ajaxURL);?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('Transaction', array('action' => 'listtax_print'));?>
	<fieldset>
 		<legend>DAFTAR RINCIAN PENYETORAN PAJAK</legend>
        <table class="input">
            <tr>
                <td colspan="2">
                <strong>Bulan - Tahun transaksi yang ingin dicetak rincian penyetoran pajaknya :</strong>
                </td>
            </tr>
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Bulan - Tahun');?>:</td>
                <td>
                    <?php
                        echo $form->month('m', date('m')) . '-';
                        echo $form->year('y', '2008', date('Y'), date('Y'), null, false);
                    ?>
                </td>
            </tr>
            
            <tr>
                <td colspan="2">
                <strong>Yang perlu diinput untuk cetak rincian penyetoran pajak :</strong>
                </td>
            </tr>
            <tr>
                <td class="label-required">Nama pejabat pembuat komitmen:<br />
                <span class="label">Misal Drs Purnomo Ananto, MM</span>
                </td>
                <td>
                <?php
                    echo $form->input('sign_name', array(
                        'id' => null, 'div' => false, 'label' => false,
                        'class' => 'sign_name',
                        'name' => 'data[Transaction][sign_name]',
                        'value' => 'Drs. Purnomo Ananto, MM'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">NIP pejabat pembuat komitmen:<br />
                <span class="label">Misal 19600919186021001</span>
                </td>
                <td>
                <?php
                    echo $form->input('sign_nip', array(
                        'id' => null, 'div' => false, 'label' => false,
                        'class' => 'sign_nip',
                        'name' => 'data[Transaction][sign_nip]',
                        'value' => '196009191986021001'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Nama bendahara pengeluaran:<br />
                <span class="label">Misal Yuliana, S. Kom</span>
                </td>
                <td>
                <?php
                    echo $form->input('treasurer_name', array(
                        'id' => null, 'div' => false, 'label' => false,
                        'class' => 'treasurer_name',
                        'name' => 'data[Transaction][treasurer_name]',
                        'value' => 'Yuliana, S. Kom'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">NIP bendahara pengeluaran:<br />
                <span class="label">Misal 198007292005012004</span>
                </td>
                <td>
                <?php
                    echo $form->input('sign_nip', array(
                        'id' => null, 'div' => false, 'label' => false,
                        'class' => 'treasurer_nip',
                        'name' => 'data[Transaction][treasurer_nip]',
                        'value' => '198007292005012004'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Print', true), array('div'=>false, 'id' => 'print'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
