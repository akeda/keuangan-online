<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('searchby',
        array("fields" => array(
                "transaction_date" => array(
                    'title' => __("Tanggal Transaksi", true), 
                    'sortable' => true
                ),
                "activity_id2" => array(
                    'title' => __("Kode", true), 
                    'sortable' => true
                ),
                "activity_id" => array(
                    'title' => __("Sub Kegiatan", true), 
                    'sortable' => true
                ),
                "unit_code_id" => array(
                    'title' => __("Unit Kerja", true),
                    'sortable' => true
                ),
                "amount_out" => array(
                    'title' => __("Pengeluaran", true),
                    'sortable' => false,
                    'wrapBefore' => '<span class="numeric_on_grid">',
                    'wrapAfter' => '</span>'
                ),
                "amount_in" => array(
                    'title' => __("Penerimaan", true),
                    'sortable' => false,
                    'wrapBefore' => '<span class="numeric_on_grid">',
                    'wrapAfter' => '</span>'
                ),
                $f => array(
                    'title' => $case,
                    'sortable' => false
                ),
                "created_by" => array(
                    'title' => __("Created By", true),
                    'sortable' => false
                ),
                "created" => array(
                    'title' => __("Created On", true),
                    'sortable' => false
                )
              ),
              "editable"  => "transaction_date",
              "editableField"  => "transaction_date",
              "assoc" => array(
                'activity_id2' => array(
                    'field' => 'code',
                    'model' => 'Activity'
                ),
                'activity_id' => array(
                    'field' => 'name',
                    'model' => 'Activity'
                ),
                'unit_code_id' => array(
                    'field' => 'name',
                    'model' => 'UnitCode'
                ),
                'created_by' => array(
                    'field' => 'name',
                    'model' => 'User'
                )
              ),
              "displayedAs" => array(
                'transaction_date' => 'date',
                'created' => 'datetime',
                'total' => 'money'
              ),
              'filter' => array(
                $f => $case
              )
        ));
?>
</div>