<?php echo $javascript->codeBlock($ajaxURL);?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('Transaction', array('action' => 'sptb_print'));?>
	<fieldset>
 		<legend>SURAT PERNYATAAN TANGGUNG JAWAB BELANJA (SPTB)</legend>
        <table class="input">
            <tr>
                <td colspan="2">
                <strong>Tahun dan MAK transaksi yang ingin dicetak SPTB-nya :</strong>
                </td>
            </tr>
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Tahun');?>:</td>
                <td>
                    <?php
                        echo $form->year('y', '2008', date('Y'), date('Y'), null, false);
                    ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Kegiatan:</td>
                <td>
                <?php
                    echo $form->select(
                        'parent_activity_id', $activities,
                        null,
                        array(
                            'id' => null,
                            'class' => 'parent_activity_id ajax_select select',
                            'rel' => '#TransactionActivityId',
                            'ref' => 'parent_id',
                            'name' => 'data[Transaction][parent_activity_id]'
                        ),
                        ''
                    );
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Sub Kegiatan:</td>
                <td>
                <?php
                    echo $form->select(
                        'activity_id', $activities,
                        null, array(
                            'id' => 'TransactionActivityId',
                            'class' => 'activity_id select',
                            'model' => 'Activity',
                            'field' => 'name',
                            'name' => 'data[Transaction][activity_id]'
                        ), 
                        ''
                    );
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">MAK:</td>
                <td>
                <?php
                    echo $form->select(
                        'activity_child_id', $activity_children,
                        null, array(
                            'id' => null,
                            'class' => 'activity_child_id select',
                            'name' => 'data[Transaction][activity_child_id]'
                        ), 
                        ''
                    );
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <strong>Yang perlu diinput untuk cetak SPTB :</strong>
                </td>
            </tr>
            <tr>
                <td class="label-required">Nomor SPTB:<br />
                <span class="label">Misal 01981 / K27.2/KU/IX/2010</span>
                </td>
                <td>
                <?php
                    echo $form->input('sptb_no', array(
                        'id' => null, 'div' => false, 'label' => false,
                        'class' => 'sptb_no',
                        'name' => 'data[Transaction][sptb_no]',
                        'value' => '01981 / K27.2/KU/IX/2010'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Tanggal SPTB:</td>
                <td>
                <?php
                    echo $form->input('sptb_date', array(
                        'id' => null, 'div' => false, 'label' => false,
                        'class' => 'sptb_date', 'type' => 'date'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Tanggal DIPA/SKO:</td>
                <td>
                <?php
                    echo $form->input('dipa_date', array(
                        'id' => null, 'div' => false, 'label' => false,
                        'class' => 'dipa_date', 'type' => 'date'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Nomor DIPA/SKO:<br />
                <span class="label">Misal 0086/023-04.2/XI/2010</span>
                </td>
                <td>
                <?php
                    echo $form->input('dipa_no', array(
                        'id' => null, 'div' => false, 'label' => false,
                        'class' => 'dipa_no',
                        'name' => 'data[Transaction][dipa_no]',
                        'value' => '0086/023-04.2/XI/2010'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Nama pejabat pembuat komitmen:<br />
                <span class="label">Misal Drs Purnomo Ananto, MM</span>
                </td>
                <td>
                <?php
                    echo $form->input('sign_name', array(
                        'id' => null, 'div' => false, 'label' => false,
                        'class' => 'sign_name',
                        'name' => 'data[Transaction][sign_name]',
                        'value' => 'Drs. Purnomo Ananto, MM'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">NIP pejabat pembuat komitmen:<br />
                <span class="label">Misal 19600919186021001</span>
                </td>
                <td>
                <?php
                    echo $form->input('sign_nip', array(
                        'id' => null, 'div' => false, 'label' => false,
                        'class' => 'sign_nip',
                        'name' => 'data[Transaction][sign_nip]',
                        'value' => '196009191986021001'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Print', true), array('div'=>false, 'id' => 'print'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
