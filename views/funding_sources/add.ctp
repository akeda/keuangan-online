<?php $javascript->link('jquery.validate.min', false); ?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('FundingSource');?>
	<fieldset>
 		<legend><?php __('Tambah Sumber Dana');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Asal Sumber Dana');?>:</td>
                <td><?php echo $form->input('name', array('div'=>false, 'label' => false, 'maxlength' => 60, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Penerima Dana');?>:</td>
                <td><?php echo $form->input('pic', array('div'=>false, 'label' => false, 'maxlength' => 60, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td><?php echo __('Unit Kerja Penerima Dana');?>:</td>
                <td>
                    <?php 
                        echo $form->select('unit_code_id', $unit_codes, null, array('div'=>false, 'label' => false));
                        echo ($form->isFieldError('unit_code_id')) ? $form->error('unit_code_id') : '';
                    ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Tgl. Terima');?>:</td>
                <td>
                    <?php 
                        echo $form->input('date_received', array('div'=>false, 'label' => false, 'class'=>'required'));
                        echo ($form->isFieldError('date_received')) ? $form->error('unit_code_id') : '';
                    ?>
                </td>
            </tr>
            <tr>
                <td><?php echo __('Sifat Dana');?>:</td>
                <td>
                    <?php 
                        echo $form->input('bind', array('div'=>false, 'label' => false)) . ' Mengikat &nbsp;';
                        echo $form->input('unbind', array('div'=>false, 'label' => false)) . ' Tidak Mengikat &nbsp;';
                        echo $form->input('pnbp', array('div'=>false, 'label' => false)) . ' PNBP &nbsp;';
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Add', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>