<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array("fields" => array(
                "name" => array(
                    'title' => __("Name", true), 
                    'sortable' => true
                ),
                "date_received" => array(
                    'title' => __("Tgl. Terima", true),
                    'sortable' => true
                ),
                "pic" => array(
                    'title' => __("Penerima", true),
                    'sortable' => true
                ),
                "unit_code_id" => array(
                    'title' => __("Unit Kerja Penerima", true),
                    'sortable' => true
                ),
                "role" => array(
                    'title' => __("Sifat", true),
                    'sortable' => false
                ),
                "created_by" => array(
                    'title' => __("Diinput oleh", true),
                    'sortable' => false
                ),
                "created" => array(
                    'title' => __("Waktu input", true),
                    'sortable' => false
                )
              ),
              "editable"  => "name",
              "assoc" => array(
                'unit_code_id' => array(
                    'field' => 'name',
                    'model' => 'UnitCode'
                ),
                'created_by' => array(
                    'field' => 'name',
                    'model' => 'User'
                )
              ),
              "displayedAs" => array(
                'date_received' => 'date',
                'created' => 'datetime'
              ),
              "if" => array(
                'role' => array(
                    'operator' => 'fieldsEqualTo',
                    'fields' => array(
                        'bind' => array(
                            'name' => 'bind',
                            'expected' => 1,
                            'showAs' => 'Mengikat'
                        ),
                        'unbind' => array(
                            'name' => 'unbind',
                            'expected' => 1,
                            'showAs' => 'Tidak mengikat'
                        ),
                        'pnbp' => array(
                            'name' => 'pnbp',
                            'expected' => 1,
                            'showAs' => 'PNBP'
                        )
                    )
                )
              )
        ));
?>
</div>