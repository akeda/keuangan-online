<h1>Buku Pajak</h1>
<table class="noborder">
    <tr>
        <td class="left bold">PENERIMAAN</td>
        <td class="center bold"><span>BULAN : <?php echo $monthname;?> TAHUN : <?php echo $year;?></span></td>
        <td class="right bold">PENYETORAN</td>
    </tr>
<table>
    <thead>
        <tr>
            <th rowspan="2" class="center" width="2%">No</th>
            <th rowspan="2" class="center">Tanggal</th>
            <th rowspan="2" class="center">No Bukti Pembukuan</th>
            <th rowspan="2" class="center">Uraian</th>
            <th colspan="5" class="center">JENIS PAJAK</th>
            <th rowspan="2" class="center">Jumlah</th>
            <th rowspan="2" class="center" width="1%">&nbsp;</th>
            <th rowspan="2" class="center" width="2%">No</th>
            <th rowspan="2" class="center">Tanggal</th>
            <th rowspan="2" class="center">No Bukti Pembukuan</th>
            <th rowspan="2" class="center">Uraian</th>
            <th colspan="5" class="center">JENIS PAJAK</th>
            <th rowspan="2" class="center">Jumlah</th>
        </tr>
        <tr>
            <th>PPN-DN</th>
            <th>PPH-21</th>
            <th>PPH-22</th>
            <th>PPH-23</th>
            <th>LAIN-LAIN</th>
            <th>PPN-DN</th>
            <th>PPH-21</th>
            <th>PPH-22</th>
            <th>PPH-23</th>
            <th>LAIN-LAIN</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $total = $total_stored = 0;
            $total_per_tax = array(
                'ppndn' => 0, 'pph21' => 0,
                'pph22' => 0, 'pph23' => 0,
                'other' => 0
            );
            $total_per_tax_stored = $total_per_tax;
        ?>
        <?php foreach ($records as $n => $record): ?>
        <?php $no = $n+1; ?>
            <tr>
                <td><?php echo $no;?></td>
                <?php if ( isset($record['JournalTax']) ): ?>
                <td>
                    <?php
                        echo $time->format('j', 
                            $record['JournalTax']['transaction_date']);
                    ?>
                </td>
                <td>
                    <?php 
                        echo $record['JournalTax']['accounting_no'];
                    ?>
                </td>
                <td>
                    <?php 
                        echo $record['JournalTax']['description'];
                    ?>
                </td>
                <td><?php echo $record['JournalTax']['tax_ppndn']; ?></td>
                <td><?php echo $record['JournalTax']['tax_pph21']; ?></td>
                <td><?php echo $record['JournalTax']['tax_pph22']; ?></td>
                <td><?php echo $record['JournalTax']['tax_pph23']; ?></td>
                <td><?php echo $record['JournalTax']['tax_other']; ?></td>
                <?php
                    foreach ( $total_per_tax as $ktpt => $tpt ) {
                        $total_per_tax[$ktpt] += $record['JournalTax']['tax_' . $ktpt];
                    }
                ?>
                <td class="numeric">
                    <?php
                        echo number_format($record['JournalTax']['total'],2,'.',',');
                        $total += $record['JournalTax']['total'];
                    ?>
                </td>
                <?php else: ?>
                <td></td><td></td><td></td><td></td>
                <td></td><td></td><td></td><td></td>
                <td></td>
                <?php endif; ?>
                
                <td>&nbsp;</td>
                
                <td><?php echo $no;?></td>
                <?php if ( isset($record['JournalTaxStored']) ): ?>
                <td>
                    <?php
                        echo $time->format('j', 
                            $record['JournalTaxStored']['date_stored']);
                    ?>
                </td>
                <td>
                    <?php
                        echo $record['JournalTaxStored']['accounting_no'];
                    ?>
                </td>
                <td>
                    <?php 
                        echo $record['JournalTaxStored']['description'];
                    ?>
                </td>
                <td><?php echo $record['JournalTaxStored']['tax_ppndn']; ?></td>
                <td><?php echo $record['JournalTaxStored']['tax_pph21']; ?></td>
                <td><?php echo $record['JournalTaxStored']['tax_pph22']; ?></td>
                <td><?php echo $record['JournalTaxStored']['tax_pph23']; ?></td>
                <td><?php echo $record['JournalTaxStored']['tax_other']; ?></td>
                <td class="numeric">
                    <?php
                        echo number_format($record['JournalTaxStored']['total'],2,'.',',');
                        $total_stored += $record['JournalTaxStored']['total'];
                    ?>
                </td>
                <?php
                    foreach ( $total_per_tax_stored as $ktpt => $tpt ) {
                        $total_per_tax_stored[$ktpt] += $record['JournalTaxStored']['tax_' . $ktpt];
                    }
                ?>
                <?php else: ?>
                <td></td><td></td><td></td><td></td>
                <td></td><td></td><td></td><td></td>
                <td></td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
        <?php if (!empty($records)): ?>
            <tr>
                <td colspan="4" class="numeric">Total</td>
                <?php
                    foreach ( $total_per_tax as $tpt ) {
                        echo '<td class="numeric">' . number_format($tpt,2,'.',',') . '</td>';
                    }
                ?>
                <td><?php echo number_format($total,2,'.',','); ?></td>
                <td>&nbsp;</td>
                <td colspan="4" class="numeric">Total</td>
                <?php
                    foreach ( $total_per_tax_stored as $tpt ) {
                        echo '<td class="numeric">' . number_format($tpt,2,'.',',') . '</td>';
                    }
                ?>
                <td><?php echo number_format($total_stored,2,'.',','); ?></td>
            </tr>
        <?php endif;?>
    </tbody>
</table>