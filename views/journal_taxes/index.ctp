<div class="<?php echo $this->params['controller']; ?> index">
<?php
    echo $this->element('tablegrid', array(
            "fields" => array(
                "date_stored" => array(
                    'title' => __('Tanggal Setor', true),
                    'sortable' => true
                ),
                "transaction_date" => array(
                    'title' => __('Tanggal Transaksi', true),
                    'sortable' => true
                ),
                "accounting_no" => array(
                    'title' => __('MAK', true),
                    'sortable' => false
                ), 
                "desciption" => array(
                    'title' => __('Uraian', true),
                    'sortable' => false
                ),
                "tax_ppndn" => array(
                    'title' => __('PPN-DN', true),
                    'sortable' => false
                ),
                "tax_pph21" => array(
                    'title' => __('PPH-21', true),
                    'sortable' => false
                ),
                "tax_pph22" => array(
                    'title' => __('PPH-22', true),
                    'sortable' => false
                ),
                "tax_pph23" => array(
                    'title' => __('PPH-23', true),
                    'sortable' => false
                ),
                "tax_other" => array(
                    'title' => __('LAIN-LAIN', true),
                    'sortable' => false
                ),
                "total" => array(
                    'title' => __('JUMLAH', true),
                    'sortable' => false
                )
            ),
            "displayedAs" => array(
                'date_stored' => 'date',
                'total' => 'money',
                'tax_ppndn' => 'money',
                'tax_pph21' => 'money',
                'tax_pph22' => 'money',
                'tax_pph23' => 'money',
                'tax_other' => 'money'
            ),
            'replacement' => array(
                'date_stored' => array(
                    '01/01/1970' => array(
                        'replaced' => '<span class="red">Belum disetorkan</span>'
                    )
                )
            ),
            "editable"  => 'accounting_no'
    ));
?>
</div>
