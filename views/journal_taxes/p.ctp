<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('JournalTax', array('action' => 'printhtml'));?>
	<fieldset>
 		<legend><?php __('Buku Pajak');?></legend>
        <table class="input">
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Bulan / Tahun');?>:</td>
                <td>
                    <?php
                        echo $form->month('m', date('m'), array(), false);
                        echo '&nbsp;-&nbsp;';
                        echo $form->year('y', '2008', date('Y'), date('Y'));
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Print', true), array('div'=>false, 'id' => 'print'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>