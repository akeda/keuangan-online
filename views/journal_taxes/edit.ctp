<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('JournalTax');?>
	<fieldset>
 		<legend><?php __('Set Waktu Setor Pajak');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><strong><?php echo __('MAK', true);?></strong></td>
                <td>
                <?php 
                    echo $form->input('accounting_no', array(
                        'div'=>false, 'label'=>false,
                        'class'=>'required', 'type' => 'hidden'
                        )
                    );
                    echo $this->data['JournalTax']['accounting_no'];
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><strong><?php echo __('Uraian', true);?></strong></td>
                <td>
                <?php 
                    echo $form->input('description', array(
                        'div'=>false, 'label'=>false,
                        'class'=>'required', 'type' => 'hidden'
                        )
                    );
                    echo $this->data['JournalTax']['description'];
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><strong><?php echo __('Pajak', true);?></strong></td>
                <td>
                <?php
                    echo $form->input('tax_ppndn', array('type' => 'hidden'));
                    echo $form->input('tax_pph21', array('type' => 'hidden'));
                    echo $form->input('tax_pph22', array('type' => 'hidden'));
                    echo $form->input('tax_pph23', array('type' => 'hidden'));
                    echo $form->input('tax_other', array('type' => 'hidden'));
                    echo "PPN-DN   : " . $this->data['JournalTax']['tax_ppndn'] . '<br />';
                    echo "PPH-21   : " . $this->data['JournalTax']['tax_pph21'] . '<br />';
                    echo "PPH-22   : " . $this->data['JournalTax']['tax_pph22'] . '<br />';
                    echo "PPH-23   : " . $this->data['JournalTax']['tax_pph23'] . '<br />';
                    echo "LAIN-LAIN: " . $this->data['JournalTax']['tax_other'] . '<br />';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><strong><?php echo __('Tgl. Setor', true);?></strong></td>
                <td>
                <?php 
                    echo $form->input('date_stored', array(
                        'div' => false, 'label' => false,
                        'class' => 'required'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Update', array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>	
</div>