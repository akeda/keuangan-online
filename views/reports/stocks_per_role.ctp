<?php 
        echo "<h1>LAPORAN PENGGUNA BARANG PER $group</h1>";
        $head = "<tr>
        <th>No</th>
        <th>Nama Barang</th>
        <th>Satuan</th>
        <th>Jumlah Permintaan</th>
        <th>Tanggal Permintaan</th>";
        
        if ( $per == 'u' ) {
            $head .= '<th>Unit Kerja</th>';
            $head .= '<th>Tgl. Dipenuhi</th>';
            $head .= '<th>Jumlah Dipenuhi</th>';
            $head .= '<th>Pemohon</th>';
        } else if ( $per == 'p' ) {
            $head .= '<th>Tgl. Dipenuhi</th>';
            $head .= '<th>Jumlah Dipenuhi</th>';
            $head .= '<th>Unit Kerja</th>';
            $head .= '<th>Komitmen</th>';
        }
        
        $head .= "</tr>";
    
    echo '<br />';
    if ( $per == 'u' ) {
        if ( isset($unitAll) ) {
            echo '<span>Unit Kerja: ' . $unitAll . '</span>';
        } else {
            echo '<span>Unit Kerja: ' . $unit_codes[$items[0]['User']['unit_code_id']] . '</span>';
        }
    } else if ( $per == 'p' ) {
        echo '<span>Nama: ' . $items[0]['User']['name'] . '</span>';
    }
    echo '<span>Per tanggal: ' . date('d/m/Y') . '</span>';
?>
<table align="center" style="width: 100%">
<thead>
    <?php echo $head;?>
</thead>
<?php
foreach ($items as $key => $item) {
        $col2 = $item['Item']['name'];
        $col3 = $units[$item['Item']['unit_id']];
        $col4 = $item['ItemOut']['total'];
        $col5 = $unit_codes[$item['User']['unit_code_id']];
?>
    <tr>
        <td><?php echo $key+1;?></td>
        <td><?php echo $col2;?></td>
        <td><?php echo $col3;?></td>
        <td><?php echo $col4;?></td>
        <td><?php echo $time->format('d/m/Y', $item['ItemOut']['date_out']);?></td>
        <?php
            if ( $per == 'p' ) {
                echo '<td>' . $time->format('d/m/Y', $item['ItemOut']['date_approved']) . '</td>';
                echo '<td>' . $item['ItemOut']['total_approved'] . '</td>';
            }
        ?>
        <td><?php echo $col5;?></td>
        <?php
            if ( $per == 'p' ) {
                echo '<td>Saya telah menyimpan arsip nama-nama pengguna barang yang telah saya terima dari gudang.</td>';
            } else if ( $per == 'u' ) {
                echo '<td>' . $time->format('d/m/Y', $item['ItemOut']['date_approved']) . '</td>';
                echo '<td>' . $item['ItemOut']['total_approved'] . '</td>';
                echo '<td>' . $item['User']['name'] . '</td>';
            }
        ?>
    </tr>
<?php
}
?>
</table>