<h1>LAPORAN STOK BARANG</h1>
<h2>Per <?php echo date('d/m/Y'); ?></h2>
<br />
<table align="center">
<thead>
    <tr>
        <th rowspan="2">No.</th>
        <th rowspan="2">Nama Barang</th>
        <th rowspan="2">Satuan</th>
        <th colspan="2" style="text-align: center">Jumlah</th>
        <th rowspan="2">Stok</th>
    </tr>
    <tr>
        <th>Penerimaan</th>
        <th>Pengeluaran</th>
    </tr>
</thead>
<tbody>
<?php
    $no = 1;
    if (!empty($items)) {
        foreach ($items as $item) {
            echo '<tr>';
            echo '<td>'.$no++.'</td>';
            echo '<td>'.$item['Item']['name'].'</td>';
            echo '<td>'.$item['Unit']['name'].'</td>';
            echo '<td>'.$item['Item']['penerimaan'].'</td>';
            echo '<td>'.$item['Item']['pengeluaran'].'</td>';
            echo '<td>'.$item['Item']['stok'].'</td>';
            echo '</tr>';
        }
    } else {
        echo '<tr><td colspan="6">Tidak ada data.</td></tr>';
    }
?>
</tbody>
</table>
<br />
<span>Barang di bawah minimum : <?php echo $minimum['item']['Item']['name']; ?></span>
<span>Barang terlaris : <?php echo $terlaris['item']['Item']['name']; ?></span>