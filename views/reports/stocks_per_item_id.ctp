<h2>Laporan Stok Berdasar Jenis Barang</h2>
<br />
<?php
    echo 'Nama Barang &nbsp; ' . $form->select('item_id', $items, null, array('div' => false, 'id' => 'item_id', 'type' => 'select'), false);
    
    echo '&nbsp; Sampai Periode &nbsp; ' . $form->select('periode', $periodes, null, array('div' => false, 'id' => 'periode', 'type' => 'select'), false);
    
    echo ' &nbsp; ' . $form->submit('Lihat', array('div' => false, 'label' => false, 'id' => 'klik'));
?>
<script type="text/javascript">
    var path = '<?php echo $path;?>';
    $(function() {
        $('#klik').click(function(e) {
            window.location = path +  $('#item_id').val() + '/' + $('#periode').val() + '/';
        });
    });
</script>