<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Activity');?>
	<fieldset>
 		<legend><?php __('Tambah Kegiatan');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Nama Kegiatan', true);?></td>
                <td><?php echo $form->input('name', array('div'=>false, 'label'=>false, 'class'=>'required', 'type' => 'textarea'));?></td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Kode', true);?></td>
                <td><?php echo $form->input('code', array('div'=>false, 'label'=>false, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Bagian Kegiatan dari', true);?></td>
                <td>
                    <?php 
                        echo $form->select('parent_id', $parent, null, '');
                    ?>
                    <br />
                    <span class="label">Isi jika kegiatan merupakan sub kegiatan dari kegiatan di atas</span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Update', array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Delete', true), array('action'=>'delete', $this->data['Activity']['id']), array('class'=>'del'), sprintf(__('Are you sure you want to delete', true) . '%s?', $this->data['Activity']['name'])) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>	
</div>