<div class="<?php echo $this->params['controller']; ?> index">
<?php
    echo $this->element('tablegrid', array(
            "fields" => array(
                "name" => array(
                    'title' => __('Nama', true),
                    'sortable' => true,
                    'before' => array(
                        'model' => 'Activity',
                        'field' => 'pad'
                    ),
                    'after' => '<br class="clear" />',
                    'wrapBefore' => '<div class="afterPad">',
                    'wrapAfter' => '</div>',
                ),
                "code" => array(
                    'title' => __('Kode', true),
                    'sortable' => true
                ), 
                "parent_name" => array(
                    'title' => __('Sub Kegiatan dari', true),
                    'sortable' => false
                )
            ),
            "assoc" => array(
                'parent_id' => array(
                    'model' => 'Activity',
                    'field' => 'name'
                )
            ),
            "editable"  => 'name'
    ));
?>
</div>
