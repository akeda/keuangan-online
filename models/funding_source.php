<?php
class FundingSource extends AppModel {
    var $validate = array(
        'name' => array(
            'required' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => '/^\w+[\w\s]?/',
                'message' => 'This field cannot be empty'
            )
        ),
        'pic' => array(
            'required' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => '/^\w+[\w\s]?/',
                'message' => 'This field cannot be empty'
            )
        ),
        'unit_code_id' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => array('vUnit'),
            'message' => 'Choose based on available options'
        ),
        'date_received' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => array('date'),
            'message' => 'Please fill with valid date'
        )
    );
    
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'created_by'
        ),
        'UnitCode'
    );
    
    function vUnit($field) {
        $exist = $this->UnitCode->find('count', array(
            'conditions' => array(
                'UnitCode.id' => $field["unit_code_id"]
            ),
            'recursive' => -1)
        );
        return $exist > 0;
    }
}
?>