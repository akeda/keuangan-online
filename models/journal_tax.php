<?php
class JournalTax extends AppModel {
    var $useTable = 'journal_taxes';
    var $belongsTo = array(
        'Transaction'
    );
    
    function getTotal($month = null, $year = null) {
        if ( is_null($month) ) {
            $month = date('m');
        }
        
        if ( is_null($year) ) {
            $year = date('Y');
        }
        
        $ret = 0;
        
        $conditions = array(
            $this->alias . '.transaction_date >=' => $year . '-' .
                    $month . '-01',
            $this->alias . '.transaction_date <=' => $year . '-' .
                    $month . '-31'
        );
        
        $t = $this->find('all', array(
            'conditions' => $conditions,
            'fields' => array('total')
        ));
        
        foreach ($t as $k => $r) {
            $ret += $r[$this->alias]['total'];
        }
        
        return $ret;
    }
}
?>