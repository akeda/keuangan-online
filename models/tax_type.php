<?php
class TaxType extends AppModel {
    var $validate = array(
        'code' => array(
            'required' => array(    
                'required' => true,
                'allowEmpty' => false,
                'rule' => '|^[a-zA-Z0-9\.\-\+\s]{1,20}$|',
                'message' => 'This field cannot be left blank and must be alphanumeric'
            ),
            'maxlength' => array(
                'rule' => array('maxLength', 20),
                'message' => 'Maximum character is 20 characters'
            ),
            'unique' => array(
                'rule' => array('unique', 'code'),
                'message' => 'Kode ini sudah dipakai'
            )
        ),
        'amount' => array(
            'required' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => array('numeric'),
                'message' => 'Wajib diisi dengan angka'
            )
        ),
    );
    
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'created_by',
            'fields' => array('id', 'name')
        )
    );
}
?>