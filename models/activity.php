<?php
class Activity extends AppModel {
	var $name = 'Activity';
    var $validate = array(
        'name' => array(
            'required' => array(
                'rule' => '/\w+[\w\s]/',
                'message' => 'This field cannot be left blank and must be alphanumeric'
            ),
            'maxlength' => array(
                'rule' => array('maxlength', 100),
                'message' => 'Maximum character is 100 characters'
            )
        ),
        'code' => array(
            'required' => array(
                'rule' => '/[\w\s]*/',
                'message' => 'Harap diisi'
            ),
            'maxlength' => array(
                'rule' => array('maxlength', 100),
                'message' => 'Maximum character is 100 characters'
            )
        )
    );
    
    var $actsAs = array('Tree');
    var $displayField = 'name';
    
    function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
        $recursive = -1;
        
        $records = $this->find('threaded');
        
        $__records = array();
        $i = 0;
        foreach ( $records as $key => $record ) {
            $__records[$i][$this->name]['id'] = $record[$this->name]['id'];
            $__records[$i][$this->name]['name'] = $record[$this->name]['name'];
            $__records[$i][$this->name]['code'] = $record[$this->name]['code'];
            $__records[$i][$this->name]['parent_id'] = $record[$this->name]['parent_id'];
            
            $__records[$i][$this->name]['deep'] = 0;
            $__records[$i][$this->name]['pad'] = '';
            $__records[$i][$this->name]['parent_name'] = '';
            
            $i++;
            $this->__iterateChildren( &$__records, $record, &$i, 1, $record[$this->name]['name']);
        }
        $this->__records = count($__records);
        
        return $__records;
    }
    
    function paginateCount() {
        return $this->find('count');
    }
    
    function getTree() {
        $recursive = -1;
        
        $records = $this->find('threaded');
        
        $__records = array();
        $i = 0;
        foreach ( $records as $key => $record ) {
            $__records[$i][$this->name]['id'] = $record[$this->name]['id'];
            $__records[$i][$this->name]['name'] = $record[$this->name]['name'];
            $__records[$i][$this->name]['code'] = $record[$this->name]['code'];
            $__records[$i][$this->name]['parent_id'] = $record[$this->name]['parent_id'];
            
            $__records[$i][$this->name]['deep'] = 0;
            $__records[$i][$this->name]['pad'] = '';
            $__records[$i][$this->name]['parent_name'] = '';
            
            $i++;
            $this->__iterateChildren( &$__records, $record, &$i, 1, $record[$this->name]['name'], '&rarr;');
        }
        
        $__rs = array();
        foreach ($__records as $__r) {
            $__rs[$__r['Activity']['id']] = $__r['Activity'];
        }
        
        return $__rs;
    }
    
    function getName($id) {
        return $this->find('first', array(
            'conditions' => array(
                'Activity.id' => $id
            ),
            'fields' => array('Activity.name')
            )
        );
    }
    
    function __iterateChildren(&$__records, $record, &$counter, $deep, $parent_name = '', $padText = '&rarr;') {
        
        if ( !empty($record['children']) ) { // if current record has children        
            foreach ( $record['children'] as $ck => $child ) {
                $__records[$counter][$this->name]['deep'] = $deep;
                
                $pad = '<div class="pad" style="margin-left:' . 10*$deep . 'px;' .
                                '">' . $padText . '</div>';                
                $__records[$counter][$this->name]['pad'] = $pad;
                
                //$parent = $this->getName($child[$this->name]['parent_id']);
                //$parent = $parent[$this->name]['name'];
                $__records[$counter][$this->name]['parent_name'] = $parent_name;
                
                $__records[$counter][$this->name]['id'] = $child[$this->name]['id'];
                $__records[$counter][$this->name]['name'] = $child[$this->name]['name'];
                $__records[$counter][$this->name]['code'] = $child[$this->name]['code'];
                $__records[$counter][$this->name]['parent_id'] = $child[$this->name]['parent_id'];
                
                $counter++;
                $this->__iterateChildren( &$__records, $child,  &$counter, ($deep+1), $child[$this->name]['name'], $padText);
            }
        } else { // no children
            
        }
        
    }
    
}
?>
