<?php
class TransactionRevision extends AppModel {
    var $belongsTo = array(
        'UnitCode', 'Activity', 'ActivityChild',
        'User' => array(
            'foreignKey' => 'created_by'
        ),
        'Transaction'
    );
}
?>