<?php
class BudgetDetail extends AppModel {
    var $validate = array(
        'activity_child_id' => array(
            'valid' => array(
                'rule' => 'vActivityChild',
                'message' => 'Wajib diisi dengan MAK yang ada'
            )
        )
    );
    
    var $belongsTo = array(
        'Budget',
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'created_by',
            'fields' => array('id', 'name')
        ),
        'ActivityChild'
    );
    
    var $hasMany = array(
        'BudgetDetailDescription' => array(
            'dependent' => true
        )
    );
    
    function vActivityChild($field) {
        return $this->ActivityChild->find('count', array(
            'conditions' => array(
                'ActivityChild.id' => $field['activity_child_id']
            )
        )) > 0;
    }
    
    function afterDelete() {
        
    }
}
?>
