<?php
class Volume extends AppModel {
    var $validate = array(
        'name' => array(
            'required' => array(    
                'required' => true,
                'allowEmpty' => false,
                'rule' => array('minlength', 1),
                'message' => 'Wajib diisi'
            ),
            'maxlength' => array(
                'rule' => array('maxLength', 20),
                'message' => 'Maximum character is 20 characters'
            ),
            'unique' => array(
                'rule' => array('unique', 'name'),
                'message' => 'Code already exists'
            )
        )
    );
}
?>