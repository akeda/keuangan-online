<?php
class ActivitiesController extends AppController {
    var $pageTitle    = 'Kegiatan dan Sub Kegiatan';
    
    function index() {
        $this->paginate['Activity']['limit'] = $this->Activity->find('count');
        parent::index();
    }
    
    function add() {
        $this->__setAdditionals();
        parent::add();
    }
    
    function edit($id) {
        $this->__setAdditionals();
        parent::edit($id);
    }
    
    function __setAdditionals() {
        $this->set('parent', $this->Activity->generateTreeList());
    }
    
    function getParent($id) {
        $this->layout = 'ajax';
        Configure::write('debug', 0);
        
        $this->set('result', $this->Activity->find('first', array(
            'conditions' => array(
                'Activity.id' => $id
            ),
            'fields' => array('Activity.name')
        )));
    }
    
    function getDeep($id = 0, $pad = '&nbsp;&nbsp;') {
        $this->layout = 'ajax';
        Configure::write('debug', 0);
        
        $path = $this->Activity->getPath($id, array('Activity.id'));

        if ( !empty($path) ) {
            $pad = str_repeat($pad, count($path)-1);
        } else {
            $pad = '';
        }
        
        $this->set('result', $pad);
    }
}
?>
