<?php
class JournalCashesController extends AppController {
    function p() {
    }
    
    function printhtml() {
        $this->layout = 'printhtml';
        // Configure::write('debug', 0);
        
        $records = array();
        if ( !empty($this->data) ) {
           $m = $this->data['m']['month']; 
           $y = $this->data['y']['year'];
           
           $records = $this->JournalCash->find('all', array(
                'conditions' => array(
                    'JournalCash.transaction_date >=' => $y . '-' . $m . '-01',
                    'JournalCash.transaction_date <=' => $y . '-' . $m . '-31'
                ),
                'recursive' => -1,
                //'order' => 'JournalCash.id ASC'
                'order' => 'JournalCash.transaction_date ASC, cash_receipt_no_in ASC, JournalCash.cash_receipt_no ASC'
           ));
           // last day of last month
           $ldaylmonth = mktime(0, 0, 0, $m, 0, $y);
            
           $total = $this->JournalCash->getTotal($m, $y);
           
           $this->set('year', $y);
           $this->set('monthname', $this->getMonthName($m));
           $this->set('records', $records);
           $this->set('total', $total);
           $this->set('ldaylmonth', $ldaylmonth);
		} else {
            $this->Session->setFlash('Invalid parameter', 'error');
            $this->redirect(array('action'=>'p'));
        }
    }
    
    function balanceinquiry() {
        $t = $this->JournalCash->getTotal(null, null, true);
        $this->set('balanceinquiry', $t['balance']);
        $this->set('date_now', date('d/m/Y'));
    }
}
?>