<?php
class TransactionsController extends AppController {
    var $pageTitle = 'Transaksi Penerimaan &mdash; Pengeluaran';
    
    function index() {
        $this->paginate['Transaction']['order'] = array('transaction_date' => 'ASC');
        parent::index();
    }
    
    function add() {
        $this->__setAdditionals();
		$this->__saving();
	}
    
    function edit($transaction_date = null) {
        if (!$transaction_date) {
            $this->Session->setFlash(__('Invalid parameter', true), 'error');
			$this->__redirect();
        }
        $this->set('transaction_date', $transaction_date);
		
        $this->__setAdditionals(1, $transaction_date);
        $this->__saving(1, $transaction_date);
        
		if (empty($this->data)) {
			$this->data = $this->{$this->modelName}->find('all', array(
                $this->modelName . '.transaction_date' => $transaction_date)
            );
            
            if (!$this->data) {
                $this->Session->setFlash(__('Invalid parameter', true), 'error');
                $this->__redirect('index');
            }
		}
    }
    
    function __saving($edit = false, $transaction_date = null) {
        if ( !empty($this->data) ) {
			$messageFlashSuccess = (isset($this->niceName) ? $this->niceName : $this->modelName) . ' ' . __('successfully added', true);
			$messageFlashError = (isset($this->niceName) ? $this->niceName : $this->modelName) . ' ' . __('cannot add this new record. Please fix the errors mentioned belows', true);
            if ( $edit ) {
                $messageFlashSuccess = (isset($this->niceName) ? $this->niceName : $this->modelName) . ' ' . __("successcully edited", true);
                $messageFlashError = (isset($this->niceName) ? $this->niceName : $this->modelName) . ' ' . __("cannot save this modification. Please fix the errors mentioned belows", true);
            }
            
            if ( isset($this->data['Transaction']['created_by']) ) {
                $created_by = $this->data['Transaction']['created_by'];
                $modified_by = $this->data['Transaction']['created_by'];
            } else {
                $modified_by = $this->data['Transaction']['modified_by'];
            }
            
            if ( $edit && $transaction_date ) {
                // get created_by, since
                // we are delete all children
                $created_by = $this->Transaction->find('first', array(
                    'conditions' => array(
                        'Transaction.transaction_date' => $transaction_date
                    ),
                    'fields' => array('Transaction.created_by'),
                    'recursive' => -1
                ));
                $created_by = $created_by['Transaction']['created_by'];
                
                if ( empty($created_by) ) {
                    $created_by = $this->data['Transaction']['modified_by'];
                }
                
                $transaction_dateOnEdit = $this->data['Transaction']['transaction_date']['year'] .
                    '-' . $this->data['Transaction']['transaction_date']['month'] .
                    '-' . $this->data['Transaction']['transaction_date']['day'];
            } else { 
            
                $transaction_date = $this->data['Transaction']['transaction_date']['year'] .
                    '-' . $this->data['Transaction']['transaction_date']['month'] . 
                    '-' . $this->data['Transaction']['transaction_date']['day'];
            } // eof if edit && transation_date
            
            // we need to delete all
            // transactions
            // whose transaction_date is $transaction_date
            $this->Transaction->deleteAll(
                array('Transaction.transaction_date' => $transaction_date),
                true,
                true
            );
            
            // on edit,
            // when we change its transaction date
            // make sure deleteAll for that transaction_date
            // is deleted first, then replace transaction_date with passed data
            if ( isset($transaction_dateOnEdit) && $transaction_dateOnEdit != $transaction_date ) {
                $transaction_date = $transaction_dateOnEdit;
            }
            
            unset($this->data['Transaction']['transaction_date']);
            unset($this->data['Transaction']['created_by']);
            unset($this->data['Transaction']['modified_by']);
            
            $receivers = $this->Transaction->CashReceiver->find('list', array(
                'fields' => array('id', 'name')
            ));
            
            // iterate each data and
            // set created_by and modified_by
            // set transaction date
            foreach ( $this->data['Transaction'] as $key => $transaction ) {
                $this->data['Transaction'][$key]['tax_description'] = $this->data['Transaction'][$key]['description'];
                if ( $this->data['Transaction'][$key]['cash_receiver_id'] &&
                     isset( $receivers[ $this->data['Transaction'][$key]['cash_receiver_id'] ] ) )
                {
                    $this->data['Transaction'][$key]['tax_description'] .= '<br />An: ';
                    $this->data['Transaction'][$key]['tax_description'] .= $receivers[ $this->data['Transaction'][$key]['cash_receiver_id'] ];
                }
                
                $this->data['Transaction'][$key]['created_by'] = $created_by;
                $this->data['Transaction'][$key]['modified_by'] = $modified_by;
                $this->data['Transaction'][$key]['transaction_date'] = $transaction_date;
                $this->data['Transaction'][$key]['amount_out'] = str_replace(',','', $transaction['amount_out'])*1;
                $this->data['Transaction'][$key]['amount_in'] = str_replace(',','', $transaction['amount_in'])*1;
                $this->data['Transaction'][$key]['reduction'] = str_replace(',','', $transaction['reduction'])*1;
                $this->data['Transaction'][$key]['tax_ppndn'] = str_replace(',','', $transaction['tax_ppndn'])*1;
                $this->data['Transaction'][$key]['tax_pph21'] = str_replace(',','', $transaction['tax_pph21'])*1;
                $this->data['Transaction'][$key]['tax_pph22'] = str_replace(',','', $transaction['tax_pph22'])*1;
                $this->data['Transaction'][$key]['tax_pph23'] = str_replace(',','', $transaction['tax_pph23'])*1;
                $this->data['Transaction'][$key]['tax_other'] = str_replace(',','', $transaction['tax_other'])*1;
                
                // set tax_amount on data
                $tax = $this->data['Transaction'][$key]['tax_ppndn'] +
                            $this->data['Transaction'][$key]['tax_pph21'] +
                            $this->data['Transaction'][$key]['tax_pph22'] +
                            $this->data['Transaction'][$key]['tax_pph23'] +
                            $this->data['Transaction'][$key]['tax_other'];
                $this->data['Transaction'][$key]['tax_amount'] = $tax;
                $reduction = $this->data['Transaction'][$key]['reduction'];
                
                // decreased by tax, if at least one tax is set
                // also decreased by reduction
                if ( $this->data['Transaction'][$key]['amount_out'] ) {
                    $this->data['Transaction'][$key]['total'] =
                        $this->data['Transaction'][$key]['amount_out']-$tax;
                } else if ( $this->data['Transaction'][$key]['amount_in'] ) {
                    $this->data['Transaction'][$key]['total'] = 
                        $this->data['Transaction'][$key]['amount_in']-($tax+$reduction);
                }
                
                // set tax code
                $tax_code = false;
                if ($this->data['Transaction'][$key]['tax_ppndn']) $tax_code = true;
                if ($this->data['Transaction'][$key]['tax_pph21']) $tax_code = true;
                if ($this->data['Transaction'][$key]['tax_pph22']) $tax_code = true;
                if ($this->data['Transaction'][$key]['tax_pph23']) $tax_code = true;
                if ($this->data['Transaction'][$key]['tax_other']) $tax_code = true;
                $this->data['Transaction'][$key]['tax_code'] = $tax_code;
                    
                // set mak code if
                // activity and activity child is set
                $this->data['Transaction'][$key]['mak_code'] = '';
                if ( $this->data['Transaction'][$key]['activity_id'] &&
                    $this->data['Transaction'][$key]['activity_child_id'] ) {
                    
                    $this->data['Transaction'][$key]['mak_code'] = 
                        $this->activities[ $this->data['Transaction'][$key]['activity_id'] ] . '/' .
                        $this->activity_children[ $this->data['Transaction'][$key]['activity_child_id'] ];
                }
                 
            } // eof foreach transaction
            
            $this->Transaction->create();
            $transactions = $this->data;
            
			if ($this->Transaction->saveAll($this->data['Transaction'])) {
                $this->Session->setFlash( $messageFlashSuccess, 'success');
                $this->__redirect();
			} else {
                $this->set('transactions', $transactions);
                $this->set('transaction_date', $transaction_date);
                
                // if failed we must delete other saved rows
                $this->Transaction->deleteAll(
                    array('Transaction.transaction_date' => $transaction_date),
                    true,
                    true
                );
                
                $this->Session->setFlash($messageFlashError, 'error');
			}
		}
    }
    
    function __setAdditionals($edit = false, $transaction_date = null) {
        // kegiatan
        $activities = $this->Transaction->Activity->generatetreelist();
        $this->activities = $this->Transaction->Activity->find('list', array(
            'fields' => array(
                'Activity.id', 'Activity.code'
            ),
            'recursive' => -1
        ));
        $activities__ = array();
        $subactivities__ = array();
        foreach ($activities as $kactv => $actv) {
            $subactivities__[$kactv] = $actv . ' (' .
                    $this->activities[$kactv] . ')';
                    
            if ( $this->Transaction->Activity->childcount($kactv, true) > 0 ) {
                $activities__[$kactv] = $actv . ' (' .
                    $this->activities[$kactv] . ')';
            } 
        }
        
        $this->set('activities', $activities__);
        
        $subactivities = array();
        if ( $edit && $transaction_date ) {
            $subactivities = $subactivities__; 
        }
        $this->set('subactivities', $subactivities);
        
        if ( $edit && $transaction_date ) {
            // get transactions
            $transactions = array('Transaction' => array());
            $this->Transaction->Behaviors->attach('Containable');
            $trans = $this->Transaction->find('all', array(
                'conditions' => array(
                    'Transaction.transaction_date' => $transaction_date
                ),
                'order' => 'Transaction.id ASC',
                'contain' => array('Activity')
            ));
            
            foreach ($trans as $k => $v) {
                $transactions['Transaction'][$k] = $v['Transaction'];
                $transactions['Transaction'][$k]['parent_activity_id'] = $v['Activity']['parent_id'];
            }
            
            $this->set('transactions', $transactions);
        }
        
        // mak
        $activity_children = array();
        $__activity_children = array();
        $ac = $this->Transaction->ActivityChild->find('all');
        foreach ( $ac as $c ) {
            $activity_children[ $c['ActivityChild']['id'] ] = $c['ActivityChild']['name'] .
                ' (' . $c['ActivityChild']['code'] . ')';
            $__activity_children[ $c['ActivityChild']['id'] ] = $c['ActivityChild']['code'];
        }
        $this->set('activity_children', $activity_children);
        $this->activity_children = $__activity_children;
        
        // unit code
        $unit_codes = $this->Transaction->UnitCode->find('list');
        $this->set('unit_codes', $unit_codes);
        
        // penerima kas untuk SPTB
        $cash_receivers = $this->Transaction->CashReceiver->find('list', array(
            'order' => 'name', 'recursive' => -1
        ));
        $this->set('cash_receivers', $cash_receivers);
        
        // set ajax URL
        $prefix = parent::__pathToController() . '/getOptions';
        $chkTransURL = parent::__pathToController() . '/istransacationexists';
        $this->set('ajaxURL', "var ajaxURL = '" . $prefix . "'; var imgLoading = '" . $this->webroot . "img/loading.gif'; var chkTransURL = '" . $chkTransURL . "'; var onEdit = '".$edit."'");
    }
    
/**
 * Search by specific evidenve no
 * @param string $case type of evidence no.
 * @return void
 */
    function searchby($case) {        
        switch ($case) {
            case 'cash':
            case 'receipt':
            case 'cash_receipt':
            case 'cash_receipt_no':
            case 'cash_receipt_no_in':
                $f  = 'cash_receipt_no';
                $f2 = 'cash_receipt_no_in';
                $caseLabel = 'No. Bukti Kas';
                break;
            case 'check':
            case 'check_no':
                $f = 'check_no';
                $caseLabel = 'No. Cek';
                break;
            case 'sp2d':
            case 'sp2d_no':
            default:
                $f = 'sp2d_no';
                $caseLabel = 'No. SP2D';
        }
        $q = '';
        
        if ( isset($this->params['url']['q']) && 
            !empty($this->params['url']['q']) ) {
            $q = $this->params['url']['q']; 
            
            $this->paginate['Transaction']['order'] = array('transaction_date' => 'ASC');
            $this->paginate['Transaction']['conditions'] = array(
                'Transaction.'.$f.' LIKE' => "%$q%"
            );
            
            if ( isset($f2) ) {
                $this->paginate['Transaction']['conditions'] = array(
                    'or' => array(
                        'Transaction.'.$f.' LIKE' => "%$q%",
                        'Transaction.'.$f2.' LIKE' => "%$q%"
                    )
                );
            }
            
            $this->set('q', $q);
            $this->set($f.'Value', $q);
        } else {
            $this->paginate['Transaction']['conditions'] = array(
                'Transaction.'.$f.' !=' => ""
            );
        }
        
        $this->set('str_fl', '?q=' . $q);
        $this->set('case', $caseLabel);
        $this->set('f', $f);
        $this->set('formgrid', Helper::url('delete_rows'));
        $this->set('records', $this->paginate());
    }

/**
 * Print buku kas umum
 */
    function p() {
    }

/**
 * The real print here
 */    
    function printhtml() {
        $this->layout = 'printhtml';
        Configure::write('debug', 0);
        
        $records = array();
        if ( !empty($this->data) ) {
           $m = $this->data['m']['month']; 
           $y = $this->data['y']['year'];
            
           $this->Transaction->Behaviors->attach('Containable');
           $records = $this->Transaction->find('all', array(
                'conditions' => array(
                    'Transaction.transaction_date >=' => $y . '-' . $m . '-01',
                    'Transaction.transaction_date <=' => $y . '-' . $m . '-31'
                ),
                //'order' => 'Transaction.transaction_date ASC, Transaction.id ASC',
                'order' => 'Transaction.transaction_date ASC, Transaction.cash_receipt_no ASC',
                'contain' => array(
                    'JournalBank' => array(
                        'order' => 'JournalBank.transaction_date ASC, JournalBank.transaction_id ASC'
                    ),
                    'JournalCash' => array(
                        'order' => 'JournalCash.transaction_date ASC, JournalCash.transaction_id ASC'
                    ),
                    'Activity' => array(
                        'fields' => array('Activity.code')
                    ),
                    'ActivityChild' => array(
                        'fields' => array('ActivityChild.code')
                    )
                )
            ));
           
            $rIn = $rOut = array(); // hold credit and debit
            $cIn = $cOut = 0; // counter for rIn/Out index 
            foreach ($records as $r) {
                $mak_code = '';
                
                if ( !empty($r['JournalBank']) ) {
                    $r['JournalBank']['debit_amount'] = $r['JournalBank']['debit_amount']*1;
                    $r['JournalBank']['credit_amount'] = $r['JournalBank']['credit_amount']*1;
                    
                    if ( $r['JournalBank']['debit_amount'] ) {
                        $r['JournalBank']['description'] = $r['JournalBank']['description'] . 
                            '<br />No. ' . $r['JournalBank']['sp2d_no'];
                        $rIn[$cIn] = $r['JournalBank'];
                        $cIn++;
                    } else if ( $r['JournalBank']['credit_amount'] ) {
                        $r['JournalBank']['description'] = $r['JournalBank']['description'] . 
                            '<br />No. Cek' . $r['JournalBank']['check_no'];
                        $rOut[$cOut] = $r['JournalBank'];
                        $cOut++;    
                    }
                }
                
                // build mak code
                if ( $r['Transaction']['activity_id'] && $r['Transaction']['activity_child_id'] ) {
                    $mak_code = $r['Activity']['code'] . '/' . 
                        $r['ActivityChild']['code'];
                }
                
                if ( !empty($r['JournalCash']) ) {
                    foreach ( $r['JournalCash'] as $r_ ) {
                        $r_['debit_amount'] = $r_['debit_amount']*1;
                        // change creadit_amount to amount_out of transaction instead of total
                        // as Mr. Heru requested
                        // $r_['credit_amount'] = $r_['credit_amount']*1;
                        $r_['credit_amount'] = $r['Transaction']['amount_out']*1;
                        $r_['mak_code'] = $mak_code;
                        
                        if ( $r_['debit_amount'] ) {
                            $rIn[$cIn] = $r_;
                            $cIn++;
                        } else if ( $r_['credit_amount'] ) {
                            $rOut[$cOut] = $r_;
                            $cOut++;
                        }
                    }
                }
            }
           
            $balanceinquiry = $this->Transaction->getTotal($m, $y);
           
            // now we adjust to
            // get same rows count
            $max = $cIn;
            $_nAdjust = 'rOut';
            if ( $cIn < $cOut ) {
                $max = $cOut;
                $_nAdjust = 'rIn';
            }
            
            for ( $i = count(${$_nAdjust}); $i < $max; $i++ ) {
                ${$_nAdjust}[$i]['transaction_date'] = '';
                ${$_nAdjust}[$i]['description'] = '';
                ${$_nAdjust}[$i]['cash_receipt_no_in'] = '';
                ${$_nAdjust}[$i]['cash_receipt_no'] = '';
                ${$_nAdjust}[$i]['mak_code'] = '';
                ${$_nAdjust}[$i]['debit_amount'] = '';
                ${$_nAdjust}[$i]['credit_amount'] = '';
            }
            // last day of last month
            $ldaylmonth = mktime(0, 0, 0, $m, 0, $y);
            
            $this->set('year', $y);
            $this->set('monthname', $this->getMonthName($m));
            $this->set('rIn', $rIn);
            $this->set('rOut', $rOut);
            $this->set('balanceinquiry', $balanceinquiry);
            $this->set('ldaylmonth', $ldaylmonth);
		} else {
            $this->Session->setFlash('Invalid parameter', 'error');
            $this->redirect(array('action'=>'p'));
        }
    }
    
    function sptb() {
        // kegiatan
        $activities = $this->Transaction->Activity->generatetreelist();
        $this->activities = $this->Transaction->Activity->find('list', array(
            'fields' => array(
                'Activity.id', 'Activity.code'
            ),
            'recursive' => -1
        ));
        $activities__ = array();
        $subactivities__ = array();
        foreach ($activities as $kactv => $actv) {
            $subactivities__[$kactv] = $actv . ' (' .
                    $this->activities[$kactv] . ')';
                    
            if ( $this->Transaction->Activity->childcount($kactv, true) > 0 ) {
                $activities__[$kactv] = $actv . ' (' .
                    $this->activities[$kactv] . ')';
            } 
        }
        
        $this->set('activities', $activities__);
        
        $subactivities = array();
        if ( $edit && $transaction_date ) {
            $subactivities = $subactivities__; 
        }
        $this->set('subactivities', $subactivities);
        
        // mak
        $activity_children = array();
        $__activity_children = array();
        $ac = $this->Transaction->ActivityChild->find('all');
        foreach ( $ac as $c ) {
            $activity_children[ $c['ActivityChild']['id'] ] = $c['ActivityChild']['name'] .
                ' (' . $c['ActivityChild']['code'] . ')';
            $__activity_children[ $c['ActivityChild']['id'] ] = $c['ActivityChild']['code'];
        }
        $this->set('activity_children', $activity_children);
        $this->activity_children = $__activity_children;
        
        // set ajax URL
        $prefix = parent::__pathToController() . '/getOptions';
        $this->set('ajaxURL', "var ajaxURL = '" . $prefix . "'; var imgLoading = '" . $this->webroot . "img/loading.gif';");
    }
    
    function sptb_print() {
        if ( !empty($this->data) ) {
            Configure::write('debug', 0);
            $this->layout = 'printhtml';
            
            $activity_id = $this->data['Transaction']['activity_id'];
            $activity_child_id = $this->data['Transaction']['activity_child_id'];
            $year = $this->data['y']['year'];
            
            $this->Transaction->Behaviors->attach('Containable');
            $transaction = $this->Transaction->find('all', array(
                'conditions' => array(
                    'Transaction.activity_id' => $activity_id,
                    'Transaction.activity_child_id' => $activity_child_id,
                    'Transaction.transaction_date >=' => $year . '-01-01',
                    'Transaction.transaction_date <=' => $year . '-12-31'
                ),
                'fields' => array(
                    'cash_receipt_no', 'cash_receipt_date',
                    'amount_out', 'description'
                ),
                'contain' => array(
                    'ActivityChild' => array(
                        'fields' => array('code')
                    ),
                    'CashReceiver' => array(
                        'fields' => array('name')
                    ),
                ),
                'order' => 'Transaction.id ASC'
            ));
            foreach ($transaction as $k => $t) {
                if ( !($t['Transaction']['amount_out']*1) ) {
                    unset($transaction[$k]);
                }
            }
            $this->set('transaction', $transaction);
            
            $mak = $this->Transaction->ActivityChild->find('first', array(
                'conditions' => array(
                    'id' => $activity_child_id
                ), 'recursive' => -1
            ));
            $this->set('mak_name', $mak['ActivityChild']['name']);
            $act = $this->Transaction->Activity->find('first', array(
                'conditions' => array(
                    'id' => $activity_id
                ), 'recursive' => -1
            ));
            
            $act_par = '';
            if ( $act['Activity']['parent_id'] ) {
                $_act_par = $this->Transaction->Activity->find('first', array(
                    'conditions' => array(
                        'id' => $act['Activity']['parent_id']
                    ), 'recursive' => -1
                ));
                $act_par = '.' . $_act_par['Activity']['code'];
            }
            $this->set('act', $act['Activity']['code'] . $act_par);
            
            
            $this->set('d', date('d'));
            $this->set('m', $this->getMonthName(date('m')));
            $this->set('y', date('Y'));
            
            $this->set('sptb_no', $this->data['Transaction']['sptb_no']);
            $sptb_date = $this->data['Transaction']['sptb_date']['day'] . ' ' .
                         $this->getMonthName($this->data['Transaction']['sptb_date']['month']) . ' ' .
                         $this->data['Transaction']['sptb_date']['year'];
            $this->set('sptb_date', $sptb_date);
            
            $this->set('dipa_no', $this->data['Transaction']['dipa_no']);
            $dipa_date = $this->data['Transaction']['dipa_date']['day'] . '-' .
                         $this->data['Transaction']['dipa_date']['month'] . '-' .
                         $this->data['Transaction']['dipa_date']['year'];
            $this->set('dipa_date', $dipa_date);
            
            $this->set('sign_name', $this->data['Transaction']['sign_name']);
            $this->set('sign_nip', $this->data['Transaction']['sign_nip']);
        } else {
            $this->redirect(array('action' => 'sptb'));
        }
    }
    
    function listtax() {
    }
    
    function listtax_print() {
        if ( !empty($this->data) ) {
            Configure::write('debug', 0);
            $this->layout = 'printhtml';
            
            $year = $this->data['y']['year'];
            $month = $this->data['m']['month'];
            
            $this->Transaction->Behaviors->attach('Containable');
            $activities = $this->Transaction->Activity->find('list', array(
                'fields' => array('id', 'code')
            ));
            $this->set('activities', $activities);
            $transaction = $this->Transaction->find('all', array(
                'conditions' => array(
                    'Transaction.transaction_date >=' => $year . '-' . $month . '-01',
                    'Transaction.transaction_date <=' => $year . '-' . $month . '-31'
                ),
                'fields' => array(
                    'tax_ppndn', 'tax_pph21', 'tax_pph22', 'tax_pph23',
                    'amount_out', 'description'
                ),
                'contain' => array(
                    'Activity' => array(
                        'fields' => array('code', 'parent_id')
                    ),
                    'ActivityChild' => array(
                        'fields' => array('code')
                    ),
                    'CashReceiver' => array(
                        'fields' => array('name')
                    ),
                ),
                'order' => 'Transaction.id ASC'
            ));
            foreach ($transaction as $k => $t) {
                if ( !($t['Transaction']['amount_out']*1) ) {
                    unset($transaction[$k]);
                    continue;
                }
                
                if ( ($t['Transaction']['tax_ppndn']*1) ||
                     ($t['Transaction']['tax_pph21']*1) ||
                     ($t['Transaction']['tax_pph22']*1) ||
                     ($t['Transaction']['tax_pph23']*1) )
                {
                    continue;
                } else {
                    unset($transaction[$k]);
                }
            }
            $this->set('transaction', $transaction);
            
            $this->set('m', $this->getMonthName($month));
            $this->set('y', $year);
            
            $this->set('_m', $this->getMonthName(date('m')));
            $this->set('_d', date('d'));
            $this->set('_y', date('Y'));
            
            $this->set('sign_name', $this->data['Transaction']['sign_name']);
            $this->set('sign_nip', $this->data['Transaction']['sign_nip']);
            
            $this->set('treasurer_name', $this->data['Transaction']['treasurer_name']);
            $this->set('treasurer_nip', $this->data['Transaction']['treasurer_nip']);
        } else {
            $this->redirect(array('action' => 'sptb'));
        }
    }
    
    function istransacationexists($transaction_date) {
        $this->layout = 'ajax';
        Configure::write('debug', 0);
        $r = '';
        $r = $this->Transaction->find('count', array(
            'conditions' => array(
                'Transaction.transaction_date' => $transaction_date
            )
        ));
        $this->set('r', $r);
    }
}
?>
