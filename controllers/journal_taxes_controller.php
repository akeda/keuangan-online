<?php
class JournalTaxesController extends AppController {
    var $uses = array('JournalTax');
    var $pageTitle = 'Pajak dan Tgl. Setor';
    var $__xname = 'JournalTax';
    
    function index() {
        $this->modelName = 'JournalTax';
        $this->set('modelName', $this->modelName);
        parent::index();
    }
    
    function edit($id) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid parameter', true), 'error');
			$this->__redirect();
        }
		$this->set('id', $id);
        
        if (!empty($this->data)) {
			$messageFlashSuccess = (isset($this->niceName) ? $this->niceName : $this->modelName) . ' ' . __("successcully edited", true);
			$messageFlashError = (isset($this->niceName) ? $this->niceName : $this->modelName) . ' ' . __("cannot save this modification. Please fix the errors mentioned belows", true);
			
            $data = array(
                'JournalTax' => array(
                    'id' => $id,
                    'date_stored' => 
                        $this->data['JournalTax']['date_stored']['year'] . '-' .
                        $this->data['JournalTax']['date_stored']['month'] . '-' .
                        $this->data['JournalTax']['date_stored']['day']
                )
            );
            
            $this->JournalTax->id = $id;
			if ($this->JournalTax->save($data, false, array('date_stored'))) {
				$this->Session->setFlash( $messageFlashSuccess, 'success');
                $this->__redirect('index');
			} else {
                
                $this->Session->setFlash($messageFlashError, 'error');
            }
		}
        
        if (empty($this->data)) {
			$this->data = $this->JournalTax->find('first', array(
                    'conditions' => array(
                        'JournalTax.id' => $id
                    )
                )
            );
            
            if (!$this->data) {
                $this->Session->setFlash(__('Invalid parameter', true), 'error');
                $this->__redirect('index');
            }
		} else {
            $this->data['JournalTax']['id'] = $id;
        }
	}
    
    function p() {
    }
    
    function printhtml() {
        $this->layout = 'printhtml';
        // Configure::write('debug', 0);
        
        $records = array();
        if ( !empty($this->data) ) {
            $m = $this->data['m']['month'];
            $y = $this->data['y']['year'];
        
            $records = $this->JournalTax->find('all', array(
                'conditions' => array(
                    'or' => array(
                        array(
                            'JournalTax.transaction_date >=' => $y . '-' . $m . '-01',
                            'JournalTax.transaction_date <=' => $y . '-' . $m . '-31'
                        ),
                        array(
                            'JournalTax.date_stored >=' => $y . '-' . $m . '-01',
                            'JournalTax.date_stored <=' => $y . '-' . $m . '-31'
                        )
                    )
                ),
                'recursive' => -1,
                'order' => 'JournalTax.id ASC, JournalTax.transaction_date ASC'
            ));
            $this->set('year', $y);
            $this->set('monthname', $this->getMonthName($m));
           
            $td = $y . '-' . $m;
            $_rs = $_rnys = 0;
            $rs = $rnys = array();
            foreach ($records as $kr => $r) {
                $rnys[$_rnys]['JournalTax'] = $r['JournalTax'];
                $rs[$_rs] = array();
                
                // make sure date_stored
                // is in current month and year
                $_td = substr($r['JournalTax']['transaction_date'],0,7);
                $_ds = substr($r['JournalTax']['date_stored'],0,7);
                
                if ( $r['JournalTax']['date_stored'] && ( $_ds == $td ) ) {
                    $rs[$_rs]['JournalTaxStored'] = $r['JournalTax'];
                    if ( $_td != $_ds ) {
                        unset($rnys[$_rnys]['JournalTax']);
                        $_rnys--;
                    }
                    $_rs++;
                }
                
                $_rnys++;
            }
            
           
            // whos has more elements?
            $max = $_rnys;
            if ( $_rs > $_rnys ) {
                $max = $_rs;
            }
            
            $rows = array();
            for ($i = 0; $i <= $max; $i++) {
                if ( isset($rnys[$i]['JournalTax']) ) {
                    $rows[$i]['JournalTax'] = $rnys[$i]['JournalTax'];
                }
                if ( isset($rs[$i]['JournalTaxStored']) ) {
                    $rows[$i]['JournalTaxStored'] = $rs[$i]['JournalTaxStored'];
                }
            }
           
            $this->set('records', $rows);
        
		} else {
            $this->Session->setFlash('Invalid parameter', 'error');
            $this->redirect(array('action'=>'p'));
        }
    }
}
?>