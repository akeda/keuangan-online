<?php
class JournalBanksController extends AppController {
    function p() {
    }
    
    function printhtml() {
        $this->layout = 'printhtml';
        // Configure::write('debug', 0);
        
        $records = array();
        if ( !empty($this->data) ) {
           $m = $this->data['m']['month']; 
           $y = $this->data['y']['year'];
           
           $records = $this->JournalBank->find('all', array(
                'conditions' => array(
                    'JournalBank.transaction_date >=' => $y . '-' . $m . '-01',
                    'JournalBank.transaction_date <=' => $y . '-' . $m . '-31'
                ),
                'recursive' => -1,
                'order' => 'JournalBank.id ASC'
           ));
           
           $total = $this->JournalBank->getTotal($m, $y);
           
           // last day of last month
           $ldaylmonth = mktime(0, 0, 0, $m, 0, $y);
           
           $this->set('year', $y);
           $this->set('monthname', $this->getMonthName($m));
           $this->set('records', $records);
           $this->set('total', $total);
           $this->set('ldaylmonth', $ldaylmonth);
		} else {
            $this->Session->setFlash('Invalid parameter', 'error');
            $this->redirect(array('action'=>'p'));
        }
    }
    
    function balanceinquiry() {
        $t = $this->JournalBank->getTotal(null, null, true);
        $this->set('balanceinquiry', $t['balance']);
        $this->set('date_now', date('d/m/Y'));
    }
}
?>