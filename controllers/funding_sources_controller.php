<?php
class FundingSourcesController extends AppController {
    var $pageTitle = 'Sumber Dana';
    
    function add() {
        $this->__setAdditionals();
        parent::add();
    }
    
    function edit($id) {
        $this->__setAdditionals();
        parent::edit($id);
    }
    
    function __setAdditionals() {
        $unit_codes = $this->FundingSource->UnitCode->find('list', array(
            'order' => array('UnitCode.name ASC')
        ));
        $this->set('unit_codes', $unit_codes);
    }
}
?>